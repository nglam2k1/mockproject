package mock.project;

import mock.project.controller.CandidateController;
import mock.project.entities.Candidate;
import mock.project.entities.Status;
import mock.project.entities.enums.StatusType;
import mock.project.service.CandidateService;
import mock.project.service.StatusService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class MockProjectNewApplicationTests {

    @Mock
    private CandidateService candidateService;

    @InjectMocks
    private CandidateController candidateController;

    @Mock
    private Model model;


    @Mock
    private StatusService statusService;


    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testSearchCandidateByStatusAndFullName_WithNoParams() {
        String result = candidateController.searchCandidateByStatusAndFullName("", "", model);
        assertEquals("redirect:/candidate-list", result);
        verifyNoInteractions(candidateService, statusService);
    }

    @Test
    public void testSearchCandidateBothNull() {
        String result = candidateController.searchCandidateByStatusAndFullName("", "", model);
        assertEquals("redirect:/candidate-list", result);
        verifyNoInteractions(candidateService, statusService);
    }

    @Test
    public void testSearchCandidateStatusNotNull() {
        String status = "Open";
        List<Status> expectedStatuses = new ArrayList<>(); // Add expected statuses here

        // Mocking the service response
        when(statusService.findAllStatusByStatusType(StatusType.CANDIDATE_STATUS))
                .thenReturn(expectedStatuses);

        String result = candidateController.searchCandidateByStatusAndFullName(status, "", model);

        assertEquals("list-candidate", result);
        verify(model).addAttribute("statuses", expectedStatuses);
    }

    @Test
    public void testSearchCandidateSearchDataNotNull() {
        String searchData = "Tran Quoc Doan";
        List<Candidate> expectedCandidates = new ArrayList<>(); // Add expected candidates here

        // Mocking the service response
        when(candidateService.findAllByStatusNameOrFullNameContainingIgnoreCase("", searchData))
                .thenReturn(expectedCandidates);

        String result = candidateController.searchCandidateByStatusAndFullName("", searchData, model);

        assertEquals("list-candidate", result);
        verify(model).addAttribute("candidates", expectedCandidates);
    }

    @Test
    public void testSearchCandidateBothNotNull() {
        String status = "Open";
        String searchData = "Tran Quoc Doan";
        List<Candidate> expectedCandidates = new ArrayList<>(); // Add expected candidates here

        // Mocking the service response
        when(candidateService.findAllByFullNameContainingIgnoreCaseAndStatusName(searchData, status))
                .thenReturn(expectedCandidates);

        String result = candidateController.searchCandidateByStatusAndFullName(status, searchData, model);

        assertEquals("list-candidate", result);
        verify(model).addAttribute("candidates", expectedCandidates);
    }

    @Test
    public void testSearchCandidateStatusAndEmptySearchData() {
        String status = "Waiting for interview";
        String searchData = "";
        List<Candidate> expectedCandidates = new ArrayList<>(); // Add expected candidates here

        // Mocking the service response
        when(candidateService.findAllByStatusNameOrFullNameContainingIgnoreCase(status, searchData))
                .thenReturn(expectedCandidates);

        String result = candidateController.searchCandidateByStatusAndFullName(status, searchData, model);

        assertEquals("list-candidate", result);
        verify(model).addAttribute("candidates", expectedCandidates);
    }

    @Test
    public void testSearchCandidateEmptyStatusAndSearchData() {
        String status = "";
        String searchData = "Tran Quoc Doan";
        List<Candidate> expectedCandidates = new ArrayList<>(); // Add expected candidates here

        // Mocking the service response
        when(candidateService.findAllByStatusNameOrFullNameContainingIgnoreCase(status, searchData))
                .thenReturn(expectedCandidates);

        String result = candidateController.searchCandidateByStatusAndFullName(status, searchData, model);

        assertEquals("list-candidate", result);
        verify(model).addAttribute("candidates", expectedCandidates);
    }

    @Test
    public void testSearchCandidateNullStatusAndEmptySearchData() {
        String status = "";
        String searchData = "";
        List<Candidate> expectedCandidates = new ArrayList<>(); // Add expected candidates here

        // Mocking the service response
        when(candidateService.findAllByStatusNameOrFullNameContainingIgnoreCase(status, searchData))
                .thenReturn(expectedCandidates);

        String result = candidateController.searchCandidateByStatusAndFullName(status, searchData, model);

        assertEquals("redirect:/candidate-list", result);
//        verify(model).addAttribute("candidates", expectedCandidates);
    }

    @Test
    public void testSearchCandidateEmptyStatusAndNullSearchData() {
        String status = "";
        String searchData = "";
        List<Candidate> expectedCandidates = new ArrayList<>(); // Add expected candidates here

        // Mocking the service response
        when(candidateService.findAllByStatusNameOrFullNameContainingIgnoreCase(status, searchData))
                .thenReturn(expectedCandidates);

        String result = candidateController.searchCandidateByStatusAndFullName(status, searchData, model);

        assertEquals("redirect:/candidate-list", result);
//        verify(model).addAttribute("candidates", expectedCandidates);
    }

    @Test
    public void testSearchCandidateNullStatusAndNullSearchData() {
        String status = "";
        String searchData = "";
        List<Candidate> expectedCandidates = new ArrayList<>(); // Add expected candidates here

        // Mocking the service response
        when(candidateService.findAllByStatusNameOrFullNameContainingIgnoreCase(status, searchData))
                .thenReturn(expectedCandidates);

        String result = candidateController.searchCandidateByStatusAndFullName(status, searchData, model);

        assertEquals("redirect:/candidate-list", result);

    }

    @Test
    public void testSearchCandidateRedirect() {
        String result = candidateController.searchCandidateByStatusAndFullName("", "", model);
        assertEquals("redirect:/candidate-list", result);
        verifyNoInteractions(candidateService, statusService);
    }

}
