
CREATE DATABASE MockProject
GO

Use MockProject
go

  INSERT INTO [dbo].[benefits] ([benefit_name])
VALUES
('Travel'),
('Hybrid working'),
('Lunch');


INSERT INTO [dbo].[skill] ([skill_name])
VALUES
    ('Java'),
    ('Nodejs'),
    ('.NET'),
    ('C++'),
    ('Business analysis'),
    ('Communication');

INSERT INTO [dbo].[level] ([level_name])
VALUES
    ('Fresher 1'),
    ('Junior 2.1'),
    ('Junior 2.2'),
    ('Senior 3.1'),
    ('Senior 3.2'),
    ('Delivery'),
    ('Leader'),
    ('Manager'),
    ('Vice Head');

INSERT INTO position (position_name) VALUES
                                         ('Backend Developer'),
                                         ('Business Analyst'),
                                         ('Tester'),
                                         ('HR'),
                                         ('Project manager');


INSERT INTO status (status_name, status_type)
VALUES
    ('Open', 'CANDIDATE_STATUS'),
    ('Waiting for interview', 'CANDIDATE_STATUS'),
    ('In-progress', 'CANDIDATE_STATUS'),
    ('Cancelled interview', 'CANDIDATE_STATUS'),
    ('Passed Interview', 'CANDIDATE_STATUS'),
    ('Failed interview', 'CANDIDATE_STATUS'),
    ('Waiting for approval', 'CANDIDATE_STATUS'),
    ('Approved offer', 'CANDIDATE_STATUS'),
    ('Rejected offer', 'CANDIDATE_STATUS'),
    ('Waiting for response', 'CANDIDATE_STATUS'),
    ('Accepted offer', 'CANDIDATE_STATUS'),
    ('Declined offer', 'CANDIDATE_STATUS'),
    ('Cancelled offer', 'CANDIDATE_STATUS'),
    ('Banned', 'CANDIDATE_STATUS'),
    ('Open', 'JOB_STATUS'),
    ('Closed','JOB_STATUS'),
    ('Activated', 'USER_STATUS'),
    ('De-activated', 'USER_STATUS'),
    ('Open', 'SCHEDULE_STATUS'),
    ('Today', 'SCHEDULE_STATUS'),
    ('Closed', 'SCHEDULE_STATUS'),
    ('Open', 'RESULT_INTERVIEW_STATUS'),
    ('Failed', 'RESULT_INTERVIEW_STATUS'),
    ('Pass', 'RESULT_INTERVIEW_STATUS'),
    ('Cancel', 'RESULT_INTERVIEW_STATUS');


INSERT INTO position (position_name) VALUES
                                         ('Backend Developer'),
                                         ('Business Analyst'),
                                         ('Tester'),
                                         ('HR'),
                                         ('Project manager');

INSERT INTO deparment (department_name)
VALUES
    ('IT'),
    ('HR'),
    ('Finance'),
    ('Communication'),
    ('Marketing'),
    ('Accounting');

        MANAGER
    • User name: DoanTQ1
    • Password: +todJ3

        RECUITER
    • User name: PhuongBT1
    • Password: fV516!

        INTERVIEWER
    • User name: HieuNT1
    • Password: c4hCwX