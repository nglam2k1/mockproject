package mock.project.controller;

import mock.project.dto.CandidateDto;
import mock.project.entities.*;
import mock.project.entities.enums.StatusType;
import mock.project.entities.enums.UserRole;
import mock.project.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class CandidateController {
    private final CandidateService candidateService;
    private final SkillsService skillsService;
    private final PositionService positionService;
    private final StatusService statusService;
    private final UserService userService;

    private static final String CREATE_CANDIDATE = "create-candidate";
    private static final String LIST_CANDIDATE = "list-candidate";

    @Autowired
    public CandidateController(CandidateService candidateService, SkillsService skillsService, PositionService positionService, StatusService statusService, UserService userService) {
        this.candidateService = candidateService;
        this.skillsService = skillsService;
        this.positionService = positionService;
        this.statusService = statusService;
        this.userService = userService;
    }


    //list dùng chung
    private void populateModelAttributes(Model model) {
        List<Skill> skills = skillsService.findAllSkills();
        List<Position> positions = positionService.findAllPositions();
        List<User> recruiters = userService.findAllUsersByRole(UserRole.RECRUITER);
        List<Status> statuses = statusService.findAllStatusByStatusType(StatusType.CANDIDATE_STATUS);

        model.addAttribute("skills", skills);
        model.addAttribute("positions", positions);
        model.addAttribute("statuses", statuses);
        model.addAttribute("recruiters", recruiters);
    }

    // view candidate list
    @GetMapping("/candidate-list")
    public String showCandidateListPage(Model model) {
        CandidateDto candidateDto = new CandidateDto();
        List<Status> statusList = statusService.findAllStatusByStatusType(StatusType.CANDIDATE_STATUS);
        model.addAttribute("candidateDto", candidateDto);
        model.addAttribute("statuses", statusList);
        return findPaginated(1, model);
    }

    //pagination
    @GetMapping("/candidate-list/page/{pageNo}")
    public String findPaginated(@PathVariable int pageNo, Model model) {
        int pageSize = 10;
        Page<Candidate> page = candidateService.findPaginated(pageNo, pageSize);
        List<Candidate> candidates = page.getContent();

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalItems", page.getTotalElements());
        model.addAttribute("candidates", candidates);
        return LIST_CANDIDATE;
    }


    //    view get status, skills, position create candidate page
    @GetMapping("/create-candidate")
    public void showCreateCandidatePage(Model model) {
        CandidateDto candidateDto = new CandidateDto();
        model.addAttribute("candidateDto", candidateDto);
        populateModelAttributes(model);
//        return CREATE_CANDIDATE;
    }

    //    save candidate from form
    @PostMapping("/create-candidate")
    @Transactional(rollbackFor = Exception.class) // Sử dụng @Transactional để quản lý transaction
    public String saveCandidate(@Valid @ModelAttribute CandidateDto candidateDto, BindingResult result, Model model) {
        try {
            // Kiểm tra lỗi và tồn tại của email và phone
            if (result.hasErrors() ||
                    candidateService.existsByEmail(candidateDto.getEmail()) ||
                    (StringUtils.hasText(candidateDto.getPhone()) && candidateService.existsByPhone(candidateDto.getPhone()))) {

                if (candidateService.existsByEmail(candidateDto.getEmail())) {
                    model.addAttribute("errorEmail", "Email already exists!");
                }

                if (candidateService.existsByPhone(candidateDto.getPhone())) {
                    model.addAttribute("errorPhone", "Phone number already exists!");
                }

                populateModelAttributes(model);
                return CREATE_CANDIDATE;
            }

            Candidate candidate = new Candidate();
            BeanUtils.copyProperties(candidateDto, candidate);

            Set<Skill> skillList = candidateDto.getSkillName().stream()
                    .map(s -> skillsService.findSkillBySkillName(s).orElse(null))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
            candidate.setSkills(skillList);

            if ("Open".equals(candidateDto.getStatusName())) {
                candidate.setStatus(statusService.findByNameAndStatusType("Open", StatusType.CANDIDATE_STATUS).orElse(null));
            } else {
                candidate.setStatus(statusService.findStatusByName(candidateDto.getStatusName()).orElse(null));
            }

            candidate.setPosition(positionService.findPositionByName(candidateDto.getPositionName()).orElse(null));
            candidate.setRecruiter(userService.findUserByUsername(candidateDto.getRecruiter()).orElse(null));
            candidate.setYearsOfExperience(candidateDto.getYearsOfExperience());
            candidate.setDob(candidateDto.getDob());

            candidateService.saveCandidate(candidate);
            return "redirect:/create-candidate?success";
        } catch (Exception e) {
            populateModelAttributes(model);
            model.addAttribute("errorSave", "An error occurred while saving the candidate.");
            return CREATE_CANDIDATE;
        }
    }

    //phương thức lấy dữ liệu theo id điền vào form để update
    @GetMapping("/update-candidate/{id}")
    public String showUpdateForm(@PathVariable Long id, Model model) {
        populateModelAttributes(model);

        Candidate candidate = candidateService.findCandidateById(id).orElse(new Candidate());
        CandidateDto candidateDto = new CandidateDto();

        BeanUtils.copyProperties(candidate, candidateDto);

        candidateDto.setPositionName(candidate.getPosition().getName());
        candidateDto.setStatusName(candidate.getStatus().getName());
        candidateDto.setRecruiter(candidate.getRecruiter().getUsername());

        Set<String> skillNameList = new HashSet<>();
        for (Skill skill : candidate.getSkills()) {
            String s = skill.getSkillName();
            skillNameList.add(s);
        }
        candidateDto.setSkillName(skillNameList);
        candidateDto.setYearsOfExperience(candidate.getYearsOfExperience());
        model.addAttribute("candidateDto", candidateDto);

        return CREATE_CANDIDATE;
    }

    //phương thức update candidate
    @Transactional(rollbackFor = Exception.class) // Sử dụng @Transactional để quản lý transaction
    @PostMapping("/create-candidate/{id}")
    public String updateCandidate(@Valid @ModelAttribute CandidateDto candidateDto, BindingResult result, @PathVariable Long id, Model model) {
        Candidate candidate = candidateService.findCandidateById(id).orElse(new Candidate());
        candidateService.deleteCandidateSkillByCandidateId(candidate.getId());
        candidateDto.setId(id);

        Set<Skill> skillList = candidateDto.getSkillName().stream()
                .map(s -> skillsService.findSkillBySkillName(s).orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());

        // Kiểm tra sự thay đổi của email và phoneNumber
        if (!candidate.getEmail().equals(candidateDto.getEmail())) {
            if (candidateService.existsByEmail(candidateDto.getEmail())) {
                model.addAttribute("errorEmail", "Email already exists!");
            }
        }
        if (!candidate.getPhone().equals(candidateDto.getPhone())) {
            if (StringUtils.hasText(candidateDto.getPhone()) && candidateService.existsByPhone(candidateDto.getPhone())) {
                model.addAttribute("errorPhone", "Phone number already exists!");
            }
        }

        if (result.hasErrors() || model.containsAttribute("errorEmail") || model.containsAttribute("errorPhone")) {
            populateModelAttributes(model);
            return CREATE_CANDIDATE;
        }
        if (candidateDto.getSkillName() != null && candidateDto.getSkillName().isEmpty()) {
            result.rejectValue("skillName", "NotEmpty");
        }

        BeanUtils.copyProperties(candidateDto, candidate);

        candidate.setSkills(skillList);
        if ("Open".equals(candidateDto.getStatusName())) {
            candidate.setStatus(statusService.findByNameAndStatusType("Open", StatusType.CANDIDATE_STATUS).orElse(null));
        } else {
            candidate.setStatus(statusService.findStatusByName(candidateDto.getStatusName()).orElse(null));
        }
        candidate.setPosition(positionService.findPositionByName(candidateDto.getPositionName()).orElse(null));
        candidate.setRecruiter(userService.findUserByUsername(candidateDto.getRecruiter()).orElse(null));
        candidate.setYearsOfExperience(candidateDto.getYearsOfExperience());
        candidate.setDob(candidateDto.getDob());

        candidateService.saveCandidate(candidate);
        return "redirect:/create-candidate?updateSuccess";
    }

    @GetMapping("/view-candidate/{id}")
    public String showViewCandidate(@PathVariable Long id, Model model) {
        populateModelAttributes(model);

        Candidate candidate = candidateService.findCandidateById(id).orElse(new Candidate());
        CandidateDto candidateDto = new CandidateDto();
        BeanUtils.copyProperties(candidate, candidateDto);

        candidateDto.setPositionName(candidate.getPosition().getName());
        candidateDto.setStatusName(candidate.getStatus().getName());
        candidateDto.setRecruiter(candidate.getRecruiter().getUsername());

        Set<String> skillNameList = new HashSet<>();
        for (Skill skill : candidate.getSkills()) {
            String s = skill.getSkillName();
            skillNameList.add(s);
        }
        candidateDto.setSkillName(skillNameList);
        candidateDto.setYearsOfExperience(candidate.getYearsOfExperience());

        model.addAttribute("candidateDto", candidateDto);
        model.addAttribute("readOnly", "readOnly");
        return CREATE_CANDIDATE;
    }

    @GetMapping("/search-candidate")
    public String searchCandidateByStatusAndFullName(@RequestParam(name = "status", required = false) String status,
                                                     @RequestParam(name = "search-data", required = false) String searchData, Model model) {

        List<Status> statuses = statusService.findAllStatusByStatusType(StatusType.CANDIDATE_STATUS);
        model.addAttribute("statuses", statuses);
        model.addAttribute("searchData", searchData);
        model.addAttribute("statusParam", status);

        List<Candidate> candidates;
        if (!status.isEmpty() && !searchData.isEmpty()) {
            candidates = candidateService.findAllByFullNameContainingIgnoreCaseAndStatusName(searchData, status);
            model.addAttribute("candidates", candidates);
        } else if (!status.isEmpty()) {
            candidates = candidateService.findAllByStatusNameOrFullNameContainingIgnoreCase(status, null);
            model.addAttribute("candidates", candidates);
        } else if (!searchData.isEmpty()) {
            candidates = candidateService.findAllByStatusNameOrFullNameContainingIgnoreCase(null, searchData);
            model.addAttribute("candidates", candidates);
        } else {
            model.addAttribute("statusParam", status);
            return "redirect:/candidate-list";
        }
        return LIST_CANDIDATE;
    }
}
