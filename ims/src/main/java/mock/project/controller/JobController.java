package mock.project.controller;

import mock.project.dto.JobDto;
import mock.project.entities.*;
import mock.project.entities.enums.StatusType;
import mock.project.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class JobController {

    private final JobService jobService;
    private final LevelService levelService;
    private final BenefitsService benefitsService;
    private final SkillService skillService;
    private final StatusService statusService;

    private static final String CREATE_JOB = "create-job";
    private static final String JOB_LIST = "job-list";

    @Autowired
    public JobController(JobService jobService, LevelService levelService, BenefitsService benefitsService, SkillService skillService, StatusService statusService) {
        this.jobService = jobService;
        this.levelService = levelService;
        this.benefitsService = benefitsService;
        this.skillService = skillService;
        this.statusService = statusService;
    }

    //view job list
    @GetMapping("/job-list")
    public String showJobListPage(Model model) {
        List<Status> statuses = statusService.findAllStatusByStatusType(StatusType.JOB_STATUS);
        model.addAttribute("jobDto", new JobDto());
        model.addAttribute("statuses", statuses);
        return findPaginated(1, model);
    }

    //    pagination
    @GetMapping("/job-list/page/{pageNo}")
    public String findPaginated(@PathVariable int pageNo, Model model) {
        int pageSize = 10;
        Page<Job> page = jobService.findPaginated(pageNo, pageSize);
        List<Job> jobs = page.getContent();

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalItems", page.getTotalElements());
        model.addAttribute("jobs", jobs);
        return JOB_LIST;
    }

    //general list
    private void populateModelAttributes(Model model) {
        List<Level> levels = levelService.findAllLevel();
        List<Benefits> benefits = benefitsService.findAllBenefit();
        List<Skill> skills = skillService.findAllSkill();
        List<Status> statuses = statusService.findAllStatusByStatusType(StatusType.JOB_STATUS);

        model.addAttribute("levels", levels);
        model.addAttribute("benefits", benefits);
        model.addAttribute("skills", skills);
        model.addAttribute("statuses", statuses);
    }


    @GetMapping("/view-job/{id}")
    public String showViewJob(@PathVariable Long id, Model model) {
        populateModelAttributes(model);
        Job job = jobService.findJobById(id).get();
        JobDto jobDto = new JobDto();

        BeanUtils.copyProperties(job, jobDto);
        jobDto.setJobTitle(job.getTitle());

        Set<String> skillNameList = new HashSet<>();
        for (Skill skill : job.getSkills()) {
            String s = skill.getSkillName();
            skillNameList.add(s);
        }

        Set<String> levelNameList = new HashSet<>();
        for (Level level : job.getLevels()) {
            String l = level.getLevelName();
            levelNameList.add(l);
        }

        Set<String> benefitNameList = new HashSet<>();
        for (Benefits benefits : job.getBenefits()) {
            String b = benefits.getBenefitName();
            benefitNameList.add(b);
        }

        jobDto.setSkillName(skillNameList);
        jobDto.setLevelName(levelNameList);
        jobDto.setBenefitName(benefitNameList);
        jobDto.setCreateTime(LocalDate.now());
        jobDto.setUpdateTime(LocalDate.now());
        model.addAttribute("jobDto", jobDto);
        model.addAttribute("readOnly", "readOnly");
        return CREATE_JOB;
    }

    //view create job page
    @GetMapping("/create-job")
    public String showCreateJobPage(Model model) {
        populateModelAttributes(model);
        JobDto jobDto = new JobDto();
        model.addAttribute("jobDto", jobDto);
        return CREATE_JOB;
    }

    //save job form
    @PostMapping("/create-job")
    @Transactional(rollbackFor = Exception.class)
    public String saveJob(@Valid @ModelAttribute JobDto jobDto, BindingResult result, Model model) {
        try {

            if (result.hasErrors()) {


                // Check if job title already exists
                if (jobService.existsByTitle(jobDto.getJobTitle())) {
                    model.addAttribute("errorTitle", "Job title already exists");
                    populateModelAttributes(model);
                    return CREATE_JOB;
                }

                // Check if salaryTo is greater than or equal to salaryFrom
                if (jobDto.getSalaryTo() != null && jobDto.getSalaryFrom() != null && jobDto.getSalaryTo().compareTo(jobDto.getSalaryFrom()) < 0) {
                    model.addAttribute("errorSalary", "Salary To must be greater than or equal to Salary From");
                    populateModelAttributes(model);
                    return CREATE_JOB;
                }

                // Check if startDate is before endDate
                if (jobDto.getStartDate().isAfter(jobDto.getEndDate())) {
                    model.addAttribute("errorDate", "Start date must be before end date");
                    populateModelAttributes(model);
                    return CREATE_JOB;
                }
                populateModelAttributes(model);
                return CREATE_JOB;
            }

            Job job = new Job();
            BeanUtils.copyProperties(jobDto, job);
            Set<Skill> skillList = jobDto.getSkillName().stream()
                    .map(s -> skillService.findSkillBySkillName(s).orElse(null))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
            job.setSkills(skillList);

            Set<Level> levelList = jobDto.getLevelName().stream()
                    .map(l -> levelService.findLevelByLevelName(l).orElse(null))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
            job.setLevels(levelList);

            Set<Benefits> benefitsList = jobDto.getBenefitName().stream()
                    .map(b -> benefitsService.findBenefitByBenefitName(b).orElse(null))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
            job.setBenefits(benefitsList);

            job.setTitle(jobDto.getJobTitle());
            job.setStartDate(jobDto.getStartDate());
            job.setEndDate(jobDto.getEndDate());
            job.setCreateTime(LocalDate.now());
            job.setUpdateTime(LocalDate.now());
            // If EndDate is less than the current date, set the status to Closed, otherwise Open
            if (job.getEndDate().isBefore(LocalDate.now())) {
                job.setStatus(statusService.findByNameAndStatusType("Closed", StatusType.JOB_STATUS).orElse(null));
            } else {
                job.setStatus(statusService.findByNameAndStatusType("Open", StatusType.JOB_STATUS).orElse(null));
            }

            jobService.saveJob(job);
            return "redirect:/create-job?success";
        } catch (Exception e) {
            model.addAttribute("errorSave", "An error occurred while saving the job.");
            return CREATE_JOB;
        }
    }

    //Lấy dữ liệu lên theo id
    @GetMapping("/update-job/{id}")
    public String showUpdateForm(@PathVariable Long id, Model model) {
        populateModelAttributes(model);
        Job job = jobService.findJobById(id).get();
        JobDto jobDto = new JobDto();

        BeanUtils.copyProperties(job, jobDto);
        jobDto.setJobTitle(job.getTitle());

        Set<String> skillNameList = new HashSet<>();
        for (Skill skill : job.getSkills()) {
            String s = skill.getSkillName();
            skillNameList.add(s);
        }

        Set<String> levelNameList = new HashSet<>();
        for (Level level : job.getLevels()) {
            String l = level.getLevelName();
            levelNameList.add(l);
        }

        Set<String> benefitNameList = new HashSet<>();
        for (Benefits benefits : job.getBenefits()) {
            String b = benefits.getBenefitName();
            benefitNameList.add(b);
        }

        jobDto.setSkillName(skillNameList);
        jobDto.setLevelName(levelNameList);
        jobDto.setBenefitName(benefitNameList);
        job.setStartDate(jobDto.getStartDate());
        job.setEndDate(jobDto.getEndDate());
        model.addAttribute("jobDto", jobDto);
        return CREATE_JOB;
    }

    //Update
    @PostMapping("/create-job/{id}")
    @Transactional(rollbackFor = Exception.class)
    public String updateJob(@Valid @ModelAttribute JobDto jobDto, BindingResult result, @PathVariable Long id, Model model) {
        Job job = jobService.findJobById(id).orElse(new Job());
        jobService.deleteJobSkillByJobId(job.getId());
        jobService.deleteJobLevelByJobId(job.getId());
        jobService.deleteJobBenefitsByJobId(job.getId());
        jobDto.setId(id);

        if (result.hasErrors()) {


            // Check if salaryTo is greater than or equal to salaryFrom
            if (jobDto.getSalaryTo() != null && jobDto.getSalaryFrom() != null && jobDto.getSalaryTo().compareTo(jobDto.getSalaryFrom()) < 0) {
                model.addAttribute("errorSalary", "Salary To must be greater than or equal to Salary From");
                populateModelAttributes(model);
                return CREATE_JOB;
            }

            // Check if startDate is before endDate
            if (jobDto.getStartDate().isAfter(jobDto.getEndDate())) {
                model.addAttribute("errorDate", "Start date must be before end date");
                populateModelAttributes(model);
                return CREATE_JOB;
            }
            populateModelAttributes(model);
            return CREATE_JOB;
        }

        BeanUtils.copyProperties(jobDto, job);
        Set<Skill> skillList = jobDto.getSkillName().stream()
                .map(s -> skillService.findSkillBySkillName(s).orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        job.setSkills(skillList);

        Set<Level> levelList = jobDto.getLevelName().stream()
                .map(l -> levelService.findLevelByLevelName(l).orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        job.setLevels(levelList);

        Set<Benefits> benefitsList = jobDto.getBenefitName().stream()
                .map(b -> benefitsService.findBenefitByBenefitName(b).orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        job.setBenefits(benefitsList);


        job.setTitle(jobDto.getJobTitle());
        job.setStartDate(jobDto.getStartDate());
        job.setEndDate(jobDto.getEndDate());
        if (job.getEndDate().isBefore(LocalDate.now())) {
            job.setStatus(statusService.findByNameAndStatusType("Closed", StatusType.JOB_STATUS).orElse(null));
        } else {
            job.setStatus(statusService.findByNameAndStatusType("Open", StatusType.JOB_STATUS).orElse(null));
        }
        job.setCreateTime(LocalDate.now());
        job.setUpdateTime(LocalDate.now());
        jobService.saveJob(job);
        return "redirect:/create-job?updateSuccess";
    }


    //delete job
    @GetMapping("/delete-job/{id}")

    public String deleteJob(@PathVariable Long id) {
        jobService.deleteJobById(id);
        return "redirect:/job-list?delete";
    }

    //search-job
    @GetMapping("/search-job")
    public String searchCandidateByStatusAndFullName(@RequestParam(name = "status", required = false) String status,
                                                     @RequestParam(name = "search-data", required = false) String searchData, Model model) {
        List<Status> statusList = statusService.findAllStatusByStatusType(StatusType.JOB_STATUS);
        List<Job> jobs;
        if (status != null && searchData != null) {
            jobs = jobService.findAllByTitleContainingIgnoreCaseAndStatusName(searchData, status);
            model.addAttribute("jobs", jobs);
        } else if (status != null) {
            jobs = jobService.findAllByStatusNameOrTitleContainingIgnoreCase(status, null);
            model.addAttribute("jobs", jobs);
        } else if (searchData != null) {
            jobs = jobService.findAllByStatusNameOrTitleContainingIgnoreCase(null, searchData);
            model.addAttribute("jobs", jobs);
        } else {
            return "redirect:/job-list";
        }
        model.addAttribute("searchData", searchData);
        model.addAttribute("statuses", statusList);
        return JOB_LIST;
    }


}
