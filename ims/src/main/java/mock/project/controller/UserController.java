package mock.project.controller;

import mock.project.dto.ForgetPasswordUserDto;
import mock.project.dto.UserDto;
import mock.project.entities.Department;
import mock.project.entities.Status;
import mock.project.entities.User;
import mock.project.entities.enums.StatusType;
import mock.project.entities.enums.UserRole;
import mock.project.service.DepartmentService;
import mock.project.service.StatusService;
import mock.project.service.UserService;
import mock.project.utils.UserUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class UserController {
    private final UserService userService;
    private final StatusService statusService;
    private final DepartmentService departmentService;
    private final UserUtils userUtils;
    private final PasswordEncoder passwordEncoder;

    private static final String CREATE_USER = "create-user";
    private static final String USER_DTO_ATTRIBUTE = "userDto";

    @Autowired

    public UserController(UserService userService, StatusService statusService, DepartmentService departmentService, UserUtils userUtils, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.statusService = statusService;
        this.departmentService = departmentService;
        this.userUtils = userUtils;
        this.passwordEncoder = passwordEncoder;
    }

    //    view user list
    @GetMapping("/user-list")
    public String showUserListPage(Model model) {
        return findPaginated(1, model);
    }

    @GetMapping("/user-list/page/{pageNo}")
    public String findPaginated(@PathVariable int pageNo, Model model) {
        int pageSize = 10;
        Page<User> page = userService.findPaginated(pageNo, pageSize);
        List<User> users = page.getContent();

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalItems", page.getTotalElements());
        model.addAttribute("users", users);
        return "user-list";
    }

    private void prepareModel(Model model) {
        List<Department> departments = departmentService.findAllDepartment();
        List<Status> statuses = statusService.findAllStatusByStatusType(StatusType.USER_STATUS);
        model.addAttribute("departments", departments);
        model.addAttribute("statuses", statuses);

    }

    //    view create user page
    @GetMapping("/create-user")
    public String showCreateUserPage(Model model) {
        UserDto userDto = new UserDto();
        model.addAttribute(USER_DTO_ATTRIBUTE, userDto);
        prepareModel(model);
        return CREATE_USER;
    }

    @GetMapping("/update-user/{id}")
    public String showUpdateForm(@PathVariable Long id, Model model) {
        User user = userService.findUserById(id).orElse( new User());
        UserDto userDto = new UserDto();
        BeanUtils.copyProperties(user, userDto);
        userDto.setDeptName(user.getDepartment().getDeptName());
        userDto.setStatusName(user.getStatus().getName());
        model.addAttribute(USER_DTO_ATTRIBUTE, userDto);
        prepareModel(model);
        return CREATE_USER;
    }

    @GetMapping("/view-user/{id}")
    public String showViewUser(@PathVariable Long id, Model model) {

        User user = userService.findUserById(id).orElse(new User());
        UserDto userDto = new UserDto();
        BeanUtils.copyProperties(user, userDto);
        model.addAttribute(USER_DTO_ATTRIBUTE, userDto);
        model.addAttribute("readOnly", "readOnly");
        prepareModel(model);
        return CREATE_USER;
    }

//    common function for save or update
    private void handleDuplicateErrors(UserDto userDto, Model model, List<Status> statuses, List<Department> departments) {
        if (userDto.getId() != null) {
            if (userService.existsByPhoneNumber(userDto.getPhoneNumber().trim())) {
                model.addAttribute("duplicatePhone", "Phone number is existed, try again!");
            }
        } else {
            if (userService.existsByEmail(userDto.getEmail())) {
                model.addAttribute("duplicateEmail", "Email is existed, try again!");
            }
            if (userService.existsByPhoneNumber(userDto.getPhoneNumber().trim())) {
                model.addAttribute("duplicatePhone", "Phone number is existed, try again!");
            }
        }
        model.addAttribute("statuses", statuses);
        model.addAttribute("departments", departments);
    }


    //    save user from form
    @PostMapping("/create-user")
    public String saveUser(@Valid @ModelAttribute UserDto userDto, BindingResult result, Model model) {
        List<Status> statuses = statusService.findAllStatusByStatusType(StatusType.USER_STATUS);
        List<Department> departments = departmentService.findAllDepartment();
        if (result.hasErrors() || userService.existsByEmail(userDto.getEmail()) || userService.existsByPhoneNumber(userDto.getPhoneNumber().trim())) {
            handleDuplicateErrors(userDto, model, statuses, departments);
            return CREATE_USER;
        }

        if (userService.existsByEmail(userDto.getEmail()) || userService.existsByPhoneNumber(userDto.getPhoneNumber())) {
            handleDuplicateErrors(userDto, model, statuses, departments);
            return CREATE_USER;
        }

        User user = new User();
        BeanUtils.copyProperties(userDto, user);
        user.setDepartment(departmentService.findDepartmentByDeptName(userDto.getDeptName()).orElse(null));
        user.setStatus(statusService.findStatusByName(userDto.getStatusName()).orElse(null));
        user.setDob(userDto.getDob());

        List<User> users = userService.findAllUser();
        List<String> existedUsernames = users.stream()
                .map(User::getUsername)
                .collect(Collectors.toList());
        String username = userUtils.autoGenerateUsername(user.getFullName(), existedUsernames);
        String password = userUtils.autoGeneratePassword(6);

//        send email
        String userEmail = user.getEmail();
        userService.sendAccountToUser(userEmail, username, password);

        user.setUsername(username);
        user.setPassword(passwordEncoder.encode(password));

        userService.saveUser(user);

        return "redirect:/create-user?success";
    }

    @PostMapping("/create-user/{id}")
    public String updateUser(@Valid @ModelAttribute("userDto") UserDto userDto, BindingResult result, @PathVariable Long id, Model model) {
        List<Status> statuses = statusService.findAllStatusByStatusType(StatusType.USER_STATUS);
        List<Department> departments = departmentService.findAllDepartment();
        User user = userService.findUserById(id).orElse(new User());

        if (result.hasErrors() || (userService.existsByPhoneNumber(userDto.getPhoneNumber().trim()) && !user.getPhoneNumber().equals(userDto.getPhoneNumber().trim()))) {
            handleDuplicateErrors(userDto, model, statuses, departments);
            return CREATE_USER;
        }

        if (userService.existsByPhoneNumber(userDto.getPhoneNumber().trim()) && !user.getPhoneNumber().equals(userDto.getPhoneNumber().trim())) {
            handleDuplicateErrors(userDto, model, statuses, departments);
            return CREATE_USER;
        }

        BeanUtils.copyProperties(userDto, user);
        user.setDepartment(departmentService.findDepartmentByDeptName(userDto.getDeptName()).orElse(null));
        user.setStatus(statusService.findStatusByName(userDto.getStatusName()).orElse(null));
        user.setDob(userDto.getDob());

        userService.saveUser(user);

        return "redirect:/create-user?updateSuccess";
    }

    @GetMapping("/search-user")
    public String searchUserByRole(@RequestParam(name = "role", required = false)String role,
                                   @RequestParam(name = "search-data", required = false) String searchData,Model model) {
        List<User> users;
        if (role != null && searchData != null) {
            users = userService.findAllUserByUsernameContainingIgnoreCaseAndRole(searchData, UserRole.valueOf(role));
        } else if (role != null) {
            users = userService.findAllUserByRoleOrUsernameContainingIgnoreCase(UserRole.valueOf(role), null);
        } else if (searchData != null) {
            users = userService.findAllUserByRoleOrUsernameContainingIgnoreCase(null, searchData);
        } else {
            return "redirect:/user-list";
        }
        model.addAttribute("users", users);
        model.addAttribute("searchData", searchData);
        return "user-list";
    }

    @PostMapping("/forgetPassword")
    public String forgetPassword(@Valid @ModelAttribute("userDto")ForgetPasswordUserDto userDto, BindingResult result) {
        if (result.hasErrors()) {
            return "forget_password";
        }
        User user = userService.findUserByEmail(userDto.getEmail()).orElse(null);
        if (user == null) {
            return "redirect:/forgetPassword?userNotFound";
        }

        String userEmail = user.getEmail();
        String username = user.getUsername();
        String password = userUtils.autoGeneratePassword(6);
        user.setPassword(passwordEncoder.encode(password));
        userService.sendNewPasswordToUser(userEmail, username, password);
        userService.saveUser(user);
        return "redirect:/forgetPassword?success";
    }
}
