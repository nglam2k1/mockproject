package mock.project.controller;

import mock.project.dto.OfferDto;
import mock.project.entities.*;
import mock.project.entities.enums.ContractType;
import mock.project.entities.enums.StatusType;
import mock.project.entities.enums.UserRole;
import mock.project.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@Controller
public class OfferController {

    private static final String CREATE_OFFER = "create-offer";
    private static final String LIST_OFFER = "list-offer";
    private static final String VIEW_OFFER = "view-offer";

    private final OfferService offerService;
    private final CandidateService candidateService;
    private final PositionService positionService;
    private final StatusService statusService;
    private final UserService userService;
    private final DepartmentService departmentService;
    private final LevelService levelService;
    private final ScheduleService scheduleService;

    @Autowired
    public OfferController(OfferService offerService, CandidateService candidateService,
                           SkillsService skillsService, PositionService positionService,
                           StatusService statusService, UserService userService,
                           DepartmentService departmentService, LevelService levelService,
                           ScheduleService scheduleService) {
        this.offerService = offerService;
        this.candidateService = candidateService;
        this.positionService = positionService;
        this.statusService = statusService;
        this.userService = userService;
        this.departmentService = departmentService;
        this.levelService = levelService;
        this.scheduleService = scheduleService;
    }

    //list dùng chung
    private void populateModelAttributes(Model model) {
        List<Position> positions = positionService.findAllPositions();
        List<User> managers = userService.findAllUsersByRole(UserRole.MANAGER);
        List<Status> statuses = statusService.findAllStatusByStatusType(StatusType.CANDIDATE_STATUS);
        List<Department> departments = departmentService.findAllDepartment();
        List<Level> levels = levelService.findAllLevel();
        List<Candidate> candidates = candidateService.findAllCandidateByStatusNameAndOfferIsNull("Passed interview");
        List<InterviewSchedule> interviewSchedules = scheduleService.findAllSchedule();
        List<User> recruiters = userService.findAllUser();

        model.addAttribute("positions", positions);
        model.addAttribute("departments", departments);
        model.addAttribute("statuses", statuses);
        model.addAttribute("managers", managers);
        model.addAttribute("levels", levels);
        model.addAttribute("candidates", candidates);
        model.addAttribute("interviewSchedules", interviewSchedules);
        model.addAttribute("recruiters", recruiters);
    }

    // view candidate list
    @GetMapping("/offer-list")
    public String showOfferListPage(Model model) {
        OfferDto offerDto = new OfferDto();
        model.addAttribute("offerDto", offerDto);
        populateModelAttributes(model);
        return findPaginated(1, model);
    }

    //pagination
    @GetMapping("/offer-list/page/{pageNo}")
    public String findPaginated(@PathVariable int pageNo, Model model) {
        int pageSize = 10;
        Page<Offer> page = offerService.findPaginated(pageNo, pageSize);
        List<Offer> offers = page.getContent();

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalItems", page.getTotalElements());
        model.addAttribute("offers", offers);
        return LIST_OFFER;
    }

    @GetMapping("/create-offer")
    public String createOffer(@ModelAttribute("offerDto") OfferDto offerDto, Model model) {
        populateModelAttributes(model);
        model.addAttribute("readOnly", "readOnly");
        return CREATE_OFFER;
    }

    @PostMapping("/create-offer")
    public String saveOffer(@Valid @ModelAttribute("offerDto") OfferDto offerDto, BindingResult result, Model model) {

        if (result.hasErrors() || (offerDto.getContractStartFrom() != null && offerDto.getContractStartFrom().isAfter(offerDto.getContractStartTo()))) {
            if (offerDto.getContractStartFrom() != null && offerDto.getContractStartFrom().isAfter(offerDto.getContractStartTo())) {
                model.addAttribute("errorDate", "ContractStartFrom date must be before ContractStartTo date");

            }
            populateModelAttributes(model);
            return CREATE_OFFER;
        }


        Offer offer = new Offer();
        BeanUtils.copyProperties(offerDto, offer);

        if ("Open".equals(offerDto.getStatusName())) {
            offer.setStatus(statusService.findByNameAndStatusType("Open", StatusType.CANDIDATE_STATUS).orElse(null));
        } else {
            offer.setStatus(statusService.findStatusByName(offerDto.getStatusName()).orElse(null));
        }

        offer.setCandidate(candidateService.findCandidateById(offerDto.getCandidateId()).orElse(null));
        offer.setManager(userService.findUserByUsername(offerDto.getManager()).orElse(null));
        offer.setContractStartFrom(offerDto.getContractStartFrom());
        offer.setContractStartTo(offerDto.getContractStartTo());
        offer.setDueDate(offerDto.getDueDate());
        offer.setLevel(levelService.findLevelByLevelName(offerDto.getLevelName()).orElse(null));
        offer.setContractType(ContractType.valueOf(offerDto.getContractType()));
        offer.setDepartment(departmentService.findDepartmentByDeptName(offerDto.getDepartment()).orElse(null));

        offerService.saveOffer(offer);
        return "redirect:/create-offer?success";
    }

    //view offers
    @GetMapping("/view-offer/{id}")
    public String showViewOffer(@PathVariable Long id, @RequestParam(name = "update-offer", required = false) Boolean editMode, Model model) {
        populateModelAttributes(model);

        Offer offer = offerService.findOfferById(id).orElse(new Offer());
        OfferDto offerDto = new OfferDto();
        BeanUtils.copyProperties(offer, offerDto);
        Candidate candidate = candidateService.findCandidateById(offer.getCandidate().getId()).orElse(null);

        offerDto.setCandidateId(offer.getCandidate().getId());
        offerDto.setCandidateFullName(offer.getCandidate().getFullName());

        offerDto.setPosition(offer.getCandidate().getPosition().getName());
        offerDto.setManager(offer.getManager().getUsername());
        offerDto.setStatusName(offer.getStatus().getName());

        Set<String> interviewersUsername = new HashSet<>();
        for (User user : offer.getCandidate().getInterviewSchedule().getInterviewers()) {
            String username = user.getUsername();
            interviewersUsername.add(username);
        }

        offerDto.setInterviewer(interviewersUsername);
        offerDto.setInterviewNotes(offer.getCandidate().getInterviewSchedule().getNote());
        offerDto.setContractType(String.valueOf(offer.getContractType()));
        offerDto.setLevelName(offer.getLevel().getLevelName());
        offerDto.setDepartment(offer.getDepartment().getDeptName());
        offerDto.setRecruiterOwner(offer.getCandidate().getRecruiter().getUsername());

        model.addAttribute("candidate", candidate);
        model.addAttribute("offerDto", offerDto);
        model.addAttribute("readOnly", "readOnly");
        // Dựa vào giá trị của tham số 'editMode', quyết định trả về view hay update
        if (editMode != null && editMode) {
            return VIEW_OFFER;
        } else {
            return CREATE_OFFER;
        }
    }

    @PostMapping("/create-offer/{id}")
    public String updateOffer(@Valid @ModelAttribute("offerDto") OfferDto offerDto, BindingResult result, @PathVariable Long id, Model model) {
        Offer offer = offerService.findOfferById(id).orElse(new Offer());
        offerDto.setId(id);

        if (result.hasErrors()) {
            populateModelAttributes(model);
            return CREATE_OFFER;
        }

        BeanUtils.copyProperties(offerDto, offer);

        if ("Open".equals(offerDto.getStatusName())) {
            offer.setStatus(statusService.findByNameAndStatusType("Open", StatusType.CANDIDATE_STATUS).orElse(null));
        } else {
            offer.setStatus(statusService.findStatusByName(offerDto.getStatusName()).orElse(null));
        }

        offer.setCandidate(candidateService.findCandidateById(offerDto.getCandidateId()).orElse(null));
        offer.setManager(userService.findUserByUsername(offerDto.getManager()).orElse(null));
        offer.setContractStartFrom(offerDto.getContractStartFrom());
        offer.setContractStartTo(offerDto.getContractStartTo());
        offer.setDueDate(offerDto.getDueDate());
        offer.setLevel(levelService.findLevelByLevelName(offerDto.getLevelName()).orElse(null));
        offer.setContractType(ContractType.valueOf(offerDto.getContractType()));
        offer.setDepartment(departmentService.findDepartmentByDeptName(offerDto.getDepartment()).orElse(null));

        offerService.saveOffer(offer);
        return "redirect:/create-offer?updateSuccess";

    }

    @GetMapping("/search-offer")
    public String searchCandidateByStatusAndFullName(@RequestParam(name = "search-data", required = false) String searchData,
                                                     @RequestParam(name = "status", required = false) String status,
                                                     @RequestParam(name = "deptName", required = false) String deptName, Model model) {

        List<Offer> offers;
        if (status.isEmpty() && deptName.isEmpty() && searchData.isEmpty()) {
            offers = offerService.findAllOffer();
            model.addAttribute("offers", offers);
        } else if (!status.isEmpty() && !deptName.isEmpty() && !searchData.isEmpty()) {
            offers = offerService.findAllByStatusNameAndDepartment_DeptNameAndCandidateFullNameContainingIgnoreCase(status, deptName, searchData);
            model.addAttribute("offers", offers);
        } else if (!status.isEmpty()) {
            offers = offerService.findAllByStatusNameOrDepartment_DeptNameOrCandidateFullNameContainingIgnoreCase(status, null, null);
            model.addAttribute("offers", offers);
        } else if (!deptName.isEmpty()) {
            offers = offerService.findAllByStatusNameOrDepartment_DeptNameOrCandidateFullNameContainingIgnoreCase(null, deptName, null);
            model.addAttribute("offers", offers);
        } else if (!searchData.isEmpty()) {
            offers = offerService.findAllByStatusNameOrDepartment_DeptNameOrCandidateFullNameContainingIgnoreCase(null, null, searchData);
            model.addAttribute("offers", offers);
        } else {
            populateModelAttributes(model);
            return "redirect:/candidate-list";
        }
        model.addAttribute("searchData", searchData);
        model.addAttribute("statusParam", status);
        model.addAttribute("deptNameParam", deptName);
        populateModelAttributes(model);
        return LIST_OFFER;
    }
}
