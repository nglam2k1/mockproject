package mock.project.controller;

import mock.project.entities.Candidate;
import mock.project.entities.User;
import mock.project.service.CandidateService;
import mock.project.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin("*")
public class DataController {
    private final CandidateService candidateService;
    private final ScheduleService scheduleService;
    @Autowired
    public DataController(CandidateService candidateService,  ScheduleService scheduleService) {
        this.candidateService = candidateService;
        this.scheduleService = scheduleService;
    }

    @GetMapping("/getRecruiterData/{candidateId}")
    @ResponseBody
    public Map<String, String> getRecruiterData(@PathVariable Long candidateId) {
        Candidate candidate = candidateService.findCandidateById(candidateId).orElse(null);
        Map<String, String> responseData = new HashMap<>();
        responseData.put("recruiterUsername", candidate.getRecruiter().getUsername());

        return responseData;
    }

    @GetMapping("/getInterviewNotesData/{candidateId}")
    @ResponseBody
    public Map<String, String> getInterviewNotesData(@PathVariable Long candidateId) {
        Candidate candidate = candidateService.findCandidateById(candidateId).orElse(null);
        Map<String, String> responseData = new HashMap<>();
        responseData.put("interviewNotes", candidate.getInterviewSchedule().getNote());

        return responseData;
    }

    @GetMapping("/getInterviewerData/{candidateId}")
    @ResponseBody
    public Map<String, List> getInterviewerData(@PathVariable Long candidateId) {
        Candidate candidate = candidateService.findCandidateById(candidateId).orElse(null);

        List<String> interviewUsernamesList = new ArrayList<>();
        for (User user : candidate.getInterviewSchedule().getInterviewers()) {
            String interviewerUsename = user.getUsername();
            interviewUsernamesList.add(interviewerUsename);
        }

        Map<String, List> responseData = new HashMap<>();
        responseData.put("interviewUsernamesList", interviewUsernamesList);
        return responseData;
    }

}
