package mock.project.controller;

import mock.project.dto.ForgetPasswordUserDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String showLoginPage() {
        return "login";
    }

    @GetMapping("/forgetPassword")
    public String showForgetPasswordPage(Model model) {
        ForgetPasswordUserDto userDto = new ForgetPasswordUserDto();
        model.addAttribute("userDto", userDto);
        return "forget_password";
    }

    @GetMapping("/home")
    public String showHomePage() {
        return "home";
    }

}
