package mock.project.controller;

import mock.project.dto.InterviewScheduleDto;
import mock.project.dto.SubmitInterViewScheduleDto;
import mock.project.dto.UpdateInterviewScheduleDto;
import mock.project.entities.Candidate;
import mock.project.entities.InterviewSchedule;
import mock.project.entities.Status;
import mock.project.entities.User;
import mock.project.entities.enums.StatusType;
import mock.project.entities.enums.UserRole;
import mock.project.service.CandidateService;
import mock.project.service.ScheduleService;
import mock.project.service.StatusService;
import mock.project.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class ScheduleController {
    private final ScheduleService scheduleService;
    private final CandidateService candidateService;
    private final UserService userService;
    private final StatusService statusService;
    private static final String CREATE_SCHEDULE = "create-schedule";
    private static final String UPDATE_SCHEDULE = "update-schedule";
    private static final String READ_ONLY_ATTRIBUTE = "readOnly";
    private static final String INTERVIEWERS_ATTRIBUTE = "interviewers";
    private static final String CREATE_ATTRIBUTE = "create";
    private static final String SCHEDULE_DTO_ATTRIBUTE = "scheduleDto";

    @Autowired
    public ScheduleController(ScheduleService scheduleService, CandidateService candidateService, UserService userService, StatusService statusService) {
        this.scheduleService = scheduleService;
        this.candidateService = candidateService;
        this.userService = userService;
        this.statusService = statusService;
    }

    @GetMapping("/schedule-list")
    public String viewSchedulePage(Model model) {
        List<User> interviewers = userService.findAllUserByRoleOrUsernameContainingIgnoreCase(UserRole.INTERVIEWER, null);
        List<Status> statuses = statusService.findAllStatusByStatusType(StatusType.RESULT_INTERVIEW_STATUS);
        model.addAttribute(INTERVIEWERS_ATTRIBUTE, interviewers);
        model.addAttribute("statuses", statuses);
        return findPaginated(1, model);
    }

    @GetMapping("/schedule-list/page/{pageNo}")
    public String findPaginated(@PathVariable int pageNo, Model model) {
        int pageSize = 10;
        Page<InterviewSchedule> page = scheduleService.findPaginated(pageNo, pageSize);
        List<InterviewSchedule> schedules = page.getContent();

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalItems", page.getTotalElements());
        model.addAttribute("schedules", schedules);
        return "schedule-list";
    }

    private void prepareScheduleView(Model model, List<User> interviewers, List<Status> results, Candidate candidate, String readOnlyAttribute) {
        model.addAttribute("results", results);
        model.addAttribute("candidate", candidate);
        model.addAttribute(INTERVIEWERS_ATTRIBUTE, interviewers);
        model.addAttribute(READ_ONLY_ATTRIBUTE, readOnlyAttribute);
    }

    private Candidate getOptionalCandidate(Long candidateId, String scheduleDtoCandidateId) {
        Optional<Candidate> candidate = candidateService.findCandidateById(candidateId);
        return candidate.orElseThrow(() -> new EntityNotFoundException("Candidate not found with ID: " + scheduleDtoCandidateId));
    }

    //    view create interview schedule page
    @GetMapping("/create-schedule")
    public String viewCreateSchedulePage(Model model) {
        List<User> interviewers = userService.findAllUserByRoleOrUsernameContainingIgnoreCase(UserRole.INTERVIEWER, null);
        List<Candidate> candidates = candidateService.findAllByStatusName("Open");

        InterviewScheduleDto scheduleDto = new InterviewScheduleDto();

        prepareScheduleView(model, interviewers, null, null, null);
        model.addAttribute(SCHEDULE_DTO_ATTRIBUTE, scheduleDto);
        model.addAttribute("candidates", candidates);
        model.addAttribute(CREATE_ATTRIBUTE, CREATE_ATTRIBUTE);

        return CREATE_SCHEDULE;
    }

    @GetMapping("/view-schedule/{id}")
    public String viewScheduleDetails(@PathVariable Long id, Model model) {
        InterviewSchedule schedule = scheduleService.findScheduleById(id).orElse(new InterviewSchedule());
        List<Status> results = statusService.findAllStatusByStatusType(StatusType.RESULT_INTERVIEW_STATUS);
        InterviewScheduleDto scheduleDto = new InterviewScheduleDto();
        BeanUtils.copyProperties(schedule, scheduleDto);
        scheduleDto.setCandidateId(schedule.getCandidate().getId());
        scheduleDto.setRecruiterName(schedule.getCandidate().getRecruiter().getUsername());

        Candidate candidate = schedule.getCandidate();
        List<User> interviewers = schedule.getInterviewers();

        List<String> interviewerUsernames = new ArrayList<>();
        for (User interviewer : schedule.getInterviewers()) {
            String s = interviewer.getUsername();
            interviewerUsernames.add(s);
        }
        scheduleDto.setInterviewerUsernames(interviewerUsernames);

        prepareScheduleView(model, interviewers, results, candidate, READ_ONLY_ATTRIBUTE);
        model.addAttribute(SCHEDULE_DTO_ATTRIBUTE, scheduleDto);
        return CREATE_SCHEDULE;
    }

    @GetMapping("/update-schedule/{id}")
    public String viewUpdateSchedule(@PathVariable Long id, Model model) {
        InterviewSchedule schedule = scheduleService.findScheduleById(id)
                .orElseThrow(() -> new EntityNotFoundException("Schedule not found with ID: " + id));
        List<User> interviewers = userService.findAllUserByRoleOrUsernameContainingIgnoreCase(UserRole.INTERVIEWER, null);
        Status result = statusService.findByNameAndStatusType("Open", StatusType.RESULT_INTERVIEW_STATUS).orElse(null);
        if (schedule.getResult().getName().equals("Cancel") || schedule.getResult().getName().equals("Open")) {
            UpdateInterviewScheduleDto scheduleDto = new UpdateInterviewScheduleDto();
            BeanUtils.copyProperties(schedule, scheduleDto);
            scheduleDto.setId(schedule.getId());
            scheduleDto.setCandidateId(schedule.getCandidate().getId());
            scheduleDto.setRecruiterName(schedule.getCandidate().getRecruiter().getUsername());
            Candidate candidate = schedule.getCandidate();

            List<String> interviewerUsernames = new ArrayList<>();
            for (User interviewer : schedule.getInterviewers()) {
                String s = interviewer.getUsername();
                interviewerUsernames.add(s);
            }
            scheduleDto.setInterviewerUsernames(interviewerUsernames);

            prepareScheduleView(model, interviewers, null, candidate, READ_ONLY_ATTRIBUTE);
            model.addAttribute(SCHEDULE_DTO_ATTRIBUTE, scheduleDto);
            model.addAttribute("result", result);

            return UPDATE_SCHEDULE;
        } else {
            return "redirect:/schedule-list?failToUpdate";
        }
    }

    @GetMapping("/submit-schedule/{id}")
    public String viewSubmitSchedule(@PathVariable Long id, Model model) {
        InterviewSchedule schedule = scheduleService.findScheduleById(id).orElse(new InterviewSchedule());

        List<Status> results = statusService.findAllStatusByStatusType(StatusType.RESULT_INTERVIEW_STATUS);
        results.remove(statusService.findByNameAndStatusType("Open", StatusType.RESULT_INTERVIEW_STATUS).orElse(null));

        SubmitInterViewScheduleDto scheduleDto = new SubmitInterViewScheduleDto();
        BeanUtils.copyProperties(schedule, scheduleDto);
        scheduleDto.setCandidateId(schedule.getCandidate().getId());
        scheduleDto.setRecruiterName(schedule.getCandidate().getRecruiter().getUsername());

        List<User> interviewers = schedule.getInterviewers();
        Candidate candidate = schedule.getCandidate();

        List<String> interviewerUsernames = new ArrayList<>();
        for (User interviewer : schedule.getInterviewers()) {
            String s = interviewer.getUsername();
            interviewerUsernames.add(s);
        }
        scheduleDto.setInterviewerUsernames(interviewerUsernames);

        prepareScheduleView(model, interviewers, results, candidate, null);
        model.addAttribute(SCHEDULE_DTO_ATTRIBUTE, scheduleDto);
        model.addAttribute("interviewerUsernames", interviewerUsernames);
        model.addAttribute("readOnly1", "readOnly1");
        model.addAttribute(CREATE_ATTRIBUTE, CREATE_ATTRIBUTE);
        return CREATE_SCHEDULE;
    }

//    save interview schedule
    @PostMapping("/create-schedule")
    public String createSchedule(@Valid @ModelAttribute("scheduleDto") InterviewScheduleDto scheduleDto, BindingResult result, Model model) {
        List<User> interviewers = userService.findAllUserByRoleOrUsernameContainingIgnoreCase(UserRole.INTERVIEWER, null);
        List<Candidate> candidates = candidateService.findAllByStatusName("Open");

        if (result.hasErrors() || (scheduleDto.getScheduleFrom() != null && scheduleDto.getScheduleTo()!= null && scheduleDto.getScheduleFrom().isAfter(scheduleDto.getScheduleTo()))) {
            if (scheduleDto.getScheduleFrom() != null && scheduleDto.getScheduleTo()!= null && scheduleDto.getScheduleFrom().isAfter(scheduleDto.getScheduleTo())) {
                model.addAttribute("invalidTime", "Start time can not be more than end time");
            }
            model.addAttribute(INTERVIEWERS_ATTRIBUTE, interviewers);
            model.addAttribute("candidates", candidates);
            model.addAttribute(CREATE_ATTRIBUTE, CREATE_ATTRIBUTE);
            return CREATE_SCHEDULE;
        }

        InterviewSchedule schedule = new InterviewSchedule();
        BeanUtils.copyProperties(scheduleDto, schedule);

//        set interviewers
        List<User> interviewerList = new ArrayList<>();
        for (String s : scheduleDto.getInterviewerUsernames()) {
            User interviewer = userService.findUserByUsername(s).orElse(null);
            interviewerList.add(interviewer);
        }
        schedule.setInterviewers(interviewerList);

//        set candidate
        Candidate candidate= getOptionalCandidate(scheduleDto.getCandidateId(), String.valueOf(scheduleDto.getCandidateId()));
        schedule.setCandidate(candidate);

        schedule.setCreateTime(LocalDate.now());
        LocalDate scheduleDate = schedule.getSchedule();

        if (scheduleDate.isEqual(LocalDate.now())) {
            schedule.setStatus(statusService.findByNameAndStatusType("Today", StatusType.SCHEDULE_STATUS).orElse(null));
            candidate.setStatus(statusService.findByNameAndStatusType("In-progress", StatusType.CANDIDATE_STATUS).orElse(null));
        } else if (scheduleDate.isAfter(LocalDate.now())) {
            schedule.setStatus(statusService.findByNameAndStatusType("Open", StatusType.SCHEDULE_STATUS).orElse(null));
            candidate.setStatus(statusService.findByNameAndStatusType("Waiting for interview", StatusType.CANDIDATE_STATUS).orElse(null));
        }
        schedule.setResult(statusService.findByNameAndStatusType("Open", StatusType.RESULT_INTERVIEW_STATUS).orElse(null));


        scheduleService.saveSchedule(schedule);
        return "redirect:/create-schedule?success";
    }

//    interviewer submit result
    @PostMapping("/create-schedule/{id}")
    public String submitSchedule(@Valid @ModelAttribute("scheduleDto") SubmitInterViewScheduleDto scheduleDto, BindingResult result, @PathVariable Long id, Model model) {
        if (result.hasErrors()) {
            return CREATE_SCHEDULE;
        }
        InterviewSchedule schedule = scheduleService.findScheduleById(id).orElse(new InterviewSchedule());
        BeanUtils.copyProperties(scheduleDto, schedule);
        schedule.setId(id);

//        set candidate
        Candidate candidate= getOptionalCandidate(scheduleDto.getCandidateId(), String.valueOf(scheduleDto.getCandidateId()));
        schedule.setCandidate(candidate);

        schedule.setResult(scheduleDto.getResult());
        if (!schedule.getResult().getName().equals("Open")) {
            if (schedule.getResult().getName().equals("Pass")) {
                candidate.setStatus(statusService.findByNameAndStatusType("Passed Interview", StatusType.CANDIDATE_STATUS).orElse(null));
            }
            if (schedule.getResult().getName().equals("Failed")) {
                candidate.setStatus(statusService.findByNameAndStatusType("Failed interview", StatusType.CANDIDATE_STATUS).orElse(null));
            }
            if (schedule.getResult().getName().equals("Cancel")) {
                candidate.setStatus(statusService.findByNameAndStatusType("Cancelled interview", StatusType.CANDIDATE_STATUS).orElse(null));
            }
            schedule.setStatus(statusService.findByNameAndStatusType("Closed", StatusType.SCHEDULE_STATUS).orElse(null));
        }

        schedule.setLocation(scheduleDto.getLocation());
        schedule.setUpdateTime(LocalDate.now());
        schedule.setUpdatedUser(SecurityContextHolder.getContext().getAuthentication().getName());

        scheduleService.saveSchedule(schedule);
        return "redirect:/create-schedule?updateSuccess";
    }

//    update schedule by MANAGER & RECRUITER
    @PostMapping("/update-schedule/{id}")
    public String updateSchedule(@Valid @ModelAttribute("scheduleDto") UpdateInterviewScheduleDto scheduleDto, BindingResult bindingResult, @PathVariable Long id, Model model) {
        List<User> interviewers = userService.findAllUserByRoleOrUsernameContainingIgnoreCase(UserRole.INTERVIEWER, null);
        Status result = statusService.findByNameAndStatusType("Open", StatusType.RESULT_INTERVIEW_STATUS).orElse(null);
        InterviewSchedule schedule = scheduleService.findScheduleById(id).orElse(new InterviewSchedule());
        BeanUtils.copyProperties(scheduleDto, schedule);
        schedule.setId(id);

        if (bindingResult.hasErrors() || (scheduleDto.getScheduleFrom() != null && scheduleDto.getScheduleTo()!= null && scheduleDto.getScheduleFrom().isAfter(scheduleDto.getScheduleTo()))) {
            if (scheduleDto.getScheduleFrom() != null && scheduleDto.getScheduleTo()!= null && scheduleDto.getScheduleFrom().isAfter(scheduleDto.getScheduleTo())) {
                model.addAttribute("invalidTime", "Start time can not be more than end time");
            }
            model.addAttribute("candidate", schedule.getCandidate());
            model.addAttribute("result", result);
            model.addAttribute(READ_ONLY_ATTRIBUTE, READ_ONLY_ATTRIBUTE);
            model.addAttribute(INTERVIEWERS_ATTRIBUTE, interviewers);
            return UPDATE_SCHEDULE;
        }

        List<User> interviewerList = new ArrayList<>();

        scheduleService.deleteInterviewersScheduleByScheduleId(id);
        for (String s : scheduleDto.getInterviewerUsernames()) {
            User interviewer = userService.findUserByUsername(s).orElse(null);
            interviewerList.add(interviewer);
        }
        schedule.setInterviewers(interviewerList);

        Candidate candidate= getOptionalCandidate(scheduleDto.getCandidateId(), String.valueOf(scheduleDto.getCandidateId()));
        schedule.setCandidate(candidate);

        schedule.setCreateTime(scheduleDto.getCreateTime());
        schedule.setResult(statusService.findByNameAndStatusType("Open", StatusType.RESULT_INTERVIEW_STATUS).orElse(null));
        schedule.setStatus(statusService.findByNameAndStatusType("Open", StatusType.SCHEDULE_STATUS).orElse(null));
        candidate.setStatus(statusService.findByNameAndStatusType("Waiting for interview", StatusType.CANDIDATE_STATUS).orElse(null));

        schedule.setLocation(scheduleDto.getLocation());
        schedule.setUpdateTime(LocalDate.now());
        schedule.setUpdatedUser(SecurityContextHolder.getContext().getAuthentication().getName());
        scheduleService.saveSchedule(schedule);
        return "redirect:/update-schedule/{id}?updateSuccess";
    }

    @GetMapping("/search-schedule")
    public String searchUserByRole(@RequestParam(name = "status", required = false) String statusName,
                                   @RequestParam(name = "search-data", required = false) String searchData,Model model) {
        List<InterviewSchedule> schedules;
        if (statusName != null && searchData != null) {
            schedules = scheduleService.findAllByTitleContainingIgnoreCaseAndStatusName(searchData, statusName);
        } else if (searchData != null) {
            schedules = scheduleService.findAllByTitleContainingIgnoreCaseOrStatusName(searchData, null);
        } else if (statusName != null) {
            schedules = scheduleService.findAllByTitleContainingIgnoreCaseOrStatusName(null, statusName);
        } else {
            return "redirect:/schedule-list";
        }

        List<Status> statuses = statusService.findAllStatusByStatusType(StatusType.RESULT_INTERVIEW_STATUS);
        model.addAttribute("statuses", statuses);
        model.addAttribute("schedules", schedules);
        model.addAttribute("searchData", searchData);
        return "schedule-list";
    }
}
