package mock.project;

import mock.project.entities.User;
import mock.project.entities.enums.UserRole;
import mock.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class MockProjectNewApplication implements CommandLineRunner {

    private final UserService userService;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public MockProjectNewApplication(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    public static void main(String[] args) {
        SpringApplication.run(MockProjectNewApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        userService.saveUser(User.builder().username("admin")
//                .password(passwordEncoder.encode("123456"))
//                .role(UserRole.ADMIN).build());

//        userService.saveUser(User.builder().username("manager")
//                .password(passwordEncoder.encode("123456"))
//                .role(UserRole.MANAGER).build());
//
//        userService.saveUser(User.builder().username("recruiter")
//                .password(passwordEncoder.encode("123456"))
//                .role(UserRole.RECRUITER).build());
//
//        userService.saveUser(User.builder().username("interviewer")
//                .password(passwordEncoder.encode("123456"))
//                .role(UserRole.INTERVIEWER).build());
    }
}
