package mock.project.dto;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class OfferDto {
    private Long id;

    @NotNull(message = "{offer.candidate.empty.invalid}")
    private Long candidateId;

    private  String candidateFullName;

    @NotEmpty(message = "{offer.manager.empty.invalid}")
    private String manager;

    private String position;

    private String interviewNotes;

    @NotEmpty(message = "{offer.status.empty.invalid}")
    private String statusName;

    private Set<String> interviewer;

    @NotNull(message = "{offer.contractStartFrom.empty.invalid}")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate contractStartFrom;

    @NotNull(message = "{offer.contractStartTo.empty.invalid}")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate contractStartTo;

    @NotEmpty(message = "{offer.contractType.empty.invalid}")
    private String contractType;

    @NotEmpty(message = "{offer.levelName.empty.invalid}")
    private String levelName;

    @NotEmpty(message = "{offer.department.empty.invalid}")
    private String department;

    private String recruiterOwner;

    @NotNull(message = "{offer.dueDate.empty.invalid}")
    @FutureOrPresent(message = "Due date must be in the present or future")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate dueDate;

    @NotNull(message = "{offer.basicSalary.empty.invalid}")
    private BigDecimal basicSalary;

    private String note;

}
