package mock.project.dto;

import lombok.*;
import mock.project.entities.Status;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class SubmitInterViewScheduleDto {
    private Long id;

    private String title;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate schedule;

    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    private LocalTime scheduleFrom;

    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    private LocalTime scheduleTo;

    private String location;

    private String meetingId;

    private String note;

    private Long candidateId;

    @NotNull(message = "{schedule.result.empty.invalid}")
    private Status result;

    private Status status;

    private String recruiterName;

    private List<String> interviewerUsernames;

    private LocalDate createTime;

    private LocalDate updateTime;

    private String updatedUser;
}
