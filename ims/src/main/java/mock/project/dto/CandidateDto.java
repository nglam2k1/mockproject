package mock.project.dto;

import lombok.*;
import mock.project.entities.enums.Gender;
import mock.project.entities.enums.HighestLevel;
import mock.project.validator.Phone;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class CandidateDto {
    private Long id;

    @NotEmpty(message = "{candidate.fullName.empty.invalid}")
    private String fullName;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate dob;

    @Phone(message = "{candidate.phone.format.invalid}")
    private String phone;

    @NotEmpty(message = "{candidate.email.empty.invalid}")
    @Email(message = "{candidate.email.format.invalid}")
    private String email;

    private String address;

    @NotNull(message = "{candidate.gender.empty.invalid}")
    private Gender gender;

    private String cvAttachment;

    @NotEmpty(message = "{candidate.position.empty.invalid}")
    private String positionName;

    @NotEmpty(message = "{candidate.status.empty.invalid}")
    private String statusName;

    @NotEmpty(message = "{candidate.skills.empty.invalid}")
    private Set<String> skillName;

    @NotEmpty(message = "{candidate.recruiter.empty.invalid}")
    private String recruiter;

    private String yearsOfExperience;

    @NotNull(message = "{candidate.highestLevel.empty.invalid}")
    private HighestLevel highestLevel;

    private String note;

}
