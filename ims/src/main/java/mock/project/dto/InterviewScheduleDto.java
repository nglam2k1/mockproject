package mock.project.dto;

import lombok.*;
import mock.project.entities.Status;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class InterviewScheduleDto {
    private Long id;

    @NotEmpty(message = "{schedule.title.empty.invalid}")
    private String title;

    @NotNull(message = "{schedule.scheduleTime.empty.invalid}")
    @FutureOrPresent(message = "{schedule.scheduleTime.past.invalid}")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate schedule;

    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    private LocalTime scheduleFrom;

    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    private LocalTime scheduleTo;

    private String location;

    private String meetingId;

    private String note;

    @NotNull(message = "{schedule.candidateName.empty.invalid}")
    private Long candidateId;

    private Status result;

    private Status status;

    private String recruiterName;

    @NotEmpty(message = "{schedule.interviewers.empty.invalid}")
    private List<String> interviewerUsernames;

    private LocalDate createTime;

    private LocalDate updateTime;

    private String updatedUser;
}
