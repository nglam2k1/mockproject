package mock.project.dto;

import lombok.*;
import mock.project.entities.enums.Gender;
import mock.project.entities.enums.UserRole;
import mock.project.validator.Phone;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class UserDto {
    private Long id;

    @NotEmpty(message = "{user.fullName.empty.invalid}")
    private String fullName;

    @NotEmpty(message = "{user.email.empty.invalid}")
    @Email(message = "{user.email.format.invalid}")
    private String email;

    @Past(message = "{user.dob.past.invalid}")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate dob;

    private String address;

    @Phone(message = "{user.phone.format.invalid}")
    private String phoneNumber;

    @NotNull(message = "{user.gender.empty.invalid}")
    private Gender gender;

    @NotNull(message = "{user.role.empty.invalid}")
    private UserRole role;

    @NotNull(message = "{user.department.empty.invalid}")
    private String deptName;

    @NotNull(message = "{user.status.empty.invalid}")
    private String statusName;

    private String note;
}
