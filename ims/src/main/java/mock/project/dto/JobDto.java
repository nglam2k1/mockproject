package mock.project.dto;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class JobDto {
    private Long id;

    @NotEmpty(message = "{job.jobTitle.empty.invalid}")
    private String jobTitle;

    @NotEmpty(message = "{job.requiredSkill.empty.invalid}")
    private Set<String> skillName;

    @NotNull(message = "Start date must be not empty!!!")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate startDate;

    @NotNull(message = "End date must be not empty!!!")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate endDate;

    @NotEmpty(message = "{job.levelName.empty.invalid}")
    private Set<String> levelName;

    private String statusName;

    private BigDecimal salaryFrom;

    @NotNull(message = "Salary To be not empty")
    private BigDecimal salaryTo;

    @NotEmpty(message = "{job.benefitName.empty.invalid}")
    private Set<String> benefitName;

    private String workingAddress;

    private String description;

    private LocalDate updateTime;

    private LocalDate createTime;
}
