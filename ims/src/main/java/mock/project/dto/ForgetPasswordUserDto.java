package mock.project.dto;

import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class ForgetPasswordUserDto {

    @NotEmpty(message = "{user.email.empty.invalid}")
    @Email(message = "{user.email.format.invalid}")
    private String email;
}
