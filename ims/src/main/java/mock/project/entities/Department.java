package mock.project.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "deparment")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class Department implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "department_id")
    private Long id;

    @Column(name = "department_name")
    private String deptName;

    @OneToMany(mappedBy = "department")
    private List<User> users;

//    one to one with offer
    @OneToMany(mappedBy = "department")
    private List<Offer> offers;



}
