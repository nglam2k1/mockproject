package mock.project.entities;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "job")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class Job {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "job_id")
    private Long id;

    @Column(name = "job_title")
    private String title;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "salary_from")
    private BigDecimal salaryFrom;

    @Column(name = "salary_to")
    private BigDecimal salaryTo;

    @Column(name = "working_address")
    private String workingAddress;

    @Column(name = "description")
    private String description;

    @Column(name = "create_time")
    private LocalDate createTime;

    @Column(name = "update_time")
    private LocalDate updateTime;

//  mapping level
    @ManyToMany
    @JoinTable(name = "job_level",
            joinColumns = {@JoinColumn(referencedColumnName = "job_id") },
            inverseJoinColumns = { @JoinColumn(
                    referencedColumnName = "level_id") })
    @ToString.Exclude
    private Set<Level> levels;

//  many to one with
    @ManyToOne
    @JoinColumn(name = "status_id" , referencedColumnName = "status_id")
    @ToString.Exclude
    private Status status;

//  many to many with SKILL
    @ManyToMany
    @JoinTable(name = "job_skill",
            joinColumns = {@JoinColumn(referencedColumnName = "job_id") },
            inverseJoinColumns = { @JoinColumn(
                    referencedColumnName = "skill_id") })
    @ToString.Exclude
    private Set<Skill> skills;

//  many to many with BENEFITS
    @ManyToMany
    @JoinTable(name = "job_benefits",
            joinColumns = {@JoinColumn(referencedColumnName = "job_id") },
            inverseJoinColumns = { @JoinColumn(
                    referencedColumnName = "benefit_id") })
    @ToString.Exclude
    private Set<Benefits> benefits;

}
