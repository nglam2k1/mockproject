package mock.project.entities;

import lombok.*;
import mock.project.entities.enums.StatusType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "status")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class Status implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "status_id")
    private Long id;

    @Column(name = "status_name")
    private String name;

    @Column(name = "status_type")
    @Enumerated(EnumType.STRING)
    private StatusType statusType;

//    one to many with candidate
    @OneToMany(mappedBy = "status")
    private Set<Candidate> candidates;

//    one to many with job
    @OneToMany(mappedBy = "status")
    private List<Job> job;

//    one to many with result schedule
    @OneToMany(mappedBy = "result")
    private List<InterviewSchedule> scheduleResults;

    @OneToMany(mappedBy = "status")
    private List<InterviewSchedule> scheduleStatus;

//   one to many with user
    @OneToMany(mappedBy = "status")
    private List<User> users;

//  one to many with offers
    @OneToMany(mappedBy = "status")
    private List<Offer> offers;
}
