package mock.project.entities;

import lombok.*;
import mock.project.entities.enums.ContractType;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "offer")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class Offer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "offer_id")
    private Long id;

    @Column(name = "note")
    private String note;

//    @Column(name = "interview_notes")
//    private String interviewNotes;

    @Column(name = "contract_type")
    @Enumerated(EnumType.STRING)
    private ContractType contractType;

    @Column(name = "due_date")
    private LocalDate dueDate;

    @Column(name = "contract_start_from")
    private LocalDate contractStartFrom;

    @Column(name = "contract_start_to")
    private LocalDate contractStartTo;

    @Column(name = "basic_salary")
    private BigDecimal basicSalary;

//    one to one with candidate (name, email, position)
    @OneToOne
    @JoinColumn(name = "candidate_id", referencedColumnName = "candidate_id")
    private Candidate candidate;

//    many to one with manager
    @ManyToOne
    @JoinColumn(name = "manager_id", referencedColumnName = "user_id")
    private User manager;

//    one to one with department
    @ManyToOne
    @JoinColumn(name = "department_id", referencedColumnName = "department_id")
    private Department department;

    @ManyToOne
    @JoinColumn(name = "status_id", referencedColumnName = "status_id")
    private Status status;

    // many to one with level
    @ManyToOne
    @JoinColumn(name = "level_id", referencedColumnName = "level_id")
    private Level level;

}
