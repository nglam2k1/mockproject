package mock.project.entities;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Entity
@Table(name = "interview_schedule")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class InterviewSchedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schedule_id")
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "schedule")
    private LocalDate schedule;

    @Column(name = "schedule_from")
    private LocalTime scheduleFrom;

    @Column(name = "schedule_to")
    private LocalTime scheduleTo;

    @Column(name = "location")
    private String location;

    @Column(name = "meeting_id")
    private String meetingId;

    @Column(name = "note")
    private String note;

    @Column(name = "create_time")
    private LocalDate createTime;

    @Column(name = "update_time")
    private LocalDate updateTime;

    @Column(name = "updated_")
    private String updatedUser;

    //mapping candidate
    @OneToOne
    @JoinColumn(name = "candidate_id", referencedColumnName = "candidate_id")
    @ToString.Exclude
    private Candidate candidate;

//    mapping result
    @ManyToOne
    @JoinColumn(name = "result_id", referencedColumnName = "status_id")
    @ToString.Exclude
    private Status result;

    //    mapping status
    @ManyToOne
    @JoinColumn(name = "status_id", referencedColumnName = "status_id")
    @ToString.Exclude
    private Status status;

    //mapping User
    @ManyToMany
    @JoinTable(name = "schedule_interviewer",
                joinColumns = @JoinColumn(name = "schedule_id"),
                inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> interviewers;


}
