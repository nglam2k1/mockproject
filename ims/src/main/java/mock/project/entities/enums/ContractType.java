package mock.project.entities.enums;

public enum ContractType {
    TWO_MONTH, THREE_MONTH, ONE_YEAR, THREE_YEAR, UNLIMITED
}
