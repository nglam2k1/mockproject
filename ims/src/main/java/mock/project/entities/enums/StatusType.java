package mock.project.entities.enums;

public enum StatusType {

    JOB_STATUS, USER_STATUS, CANDIDATE_STATUS, SCHEDULE_STATUS, RESULT_INTERVIEW_STATUS
}
