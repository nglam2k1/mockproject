package mock.project.entities.enums;

public enum Gender {
    MALE, FEMALE, OTHERS
}
