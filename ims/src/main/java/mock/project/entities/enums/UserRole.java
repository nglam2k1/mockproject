package mock.project.entities.enums;

public enum UserRole {
    ADMIN, MANAGER, INTERVIEWER, RECRUITER
}
