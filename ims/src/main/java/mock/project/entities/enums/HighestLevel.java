package mock.project.entities.enums;

public enum HighestLevel {
    HIGH_SCHOOL, BACHELOR_DEGREE, MASTER_DEGREE_PHD
}
