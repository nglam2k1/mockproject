package mock.project.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "skill")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class Skill {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "skill_id")
    private Long id;

    @Column(name = "skill_name")
    private String skillName;

    //mapping candidate
    @ManyToMany(mappedBy = "skills")
    private Set<Candidate> candidates;

    //mapping job
    @ManyToMany(mappedBy = "skills")
    private Set<Job> jobs;

}
