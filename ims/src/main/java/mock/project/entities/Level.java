package mock.project.entities;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "level")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class Level {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "level_id")
    private Long id;

    @Column(name = "level_name")
    private String levelName;

    //mapping job
    @ManyToMany(mappedBy = "levels")
    private Set<Job> jobs;

    //One to many with offer
    @OneToMany(mappedBy = "level")
    private List<Offer> offers;

}
