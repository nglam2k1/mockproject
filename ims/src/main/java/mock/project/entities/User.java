package mock.project.entities;

import lombok.*;
import mock.project.entities.enums.Gender;
import mock.project.entities.enums.UserRole;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "dob")
    private LocalDate dob;

    @Column(name = "address")
    private String address;

    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private UserRole role;

    @Column(name = "note")
    private String note;

    @ManyToOne
    @JoinColumn(name = "department_id", referencedColumnName = "department_id")
    @ToString.Exclude
    private Department department;

    @ManyToOne
    @JoinColumn(name = "status_id", referencedColumnName = "status_id")
    @ToString.Exclude
    private Status status;

//    many to many with schedule
    @ManyToMany(mappedBy = "interviewers")
    @ToString.Exclude
    private List<InterviewSchedule> interviewSchedules;

//    one to many with offers
    @OneToMany(mappedBy = "manager")
    @ToString.Exclude
    private List<Offer> offers;

    @OneToMany(mappedBy = "recruiter")
    private List<Candidate> candidates;
}
