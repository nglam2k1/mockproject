package mock.project.entities;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "benefits")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class Benefits {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "benefit_id")
    private Long id;

    @Column(name = "benefit_name")
    private String benefitName;

//    many to many with JOB
    @ManyToMany(mappedBy = "benefits")
    private Set<Job> jobs;
}
