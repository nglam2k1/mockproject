package mock.project.entities;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "position")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class Position {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "position_id")
    private Long id;

    @Column(name = "position_name")
    private String name;

//    //mapping candidate
    @OneToMany(mappedBy = "position")
    private List<Candidate> candidate;
}
