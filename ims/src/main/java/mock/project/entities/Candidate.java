package mock.project.entities;

import lombok.*;
import mock.project.entities.enums.Gender;
import mock.project.entities.enums.HighestLevel;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "candidate")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class Candidate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "candidate_id")
    private Long id;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "dob")
    private LocalDate dob;

    @Column(name = "years_of_experience")
    private String yearsOfExperience;

    @Column(name = "note")
    private String note;

    @Column(name = "highest_level")
    @Enumerated
    private HighestLevel highestLevel;

    @Column(name = "cv_attachment")
    private String cvAttachment;

    @Column(name = "address")
    private String address;

    //mapping skill
    @ManyToMany
    @JoinTable(name = "candidate_skill",
            joinColumns = {@JoinColumn(referencedColumnName = "candidate_id") },
            inverseJoinColumns = { @JoinColumn(
                    referencedColumnName = "skill_id") })
    @ToString.Exclude
    private Set<Skill> skills;

    //mapping position
    @ManyToOne
    @JoinColumn(name = "position_id", referencedColumnName = "position_id")
    @ToString.Exclude
    private Position position;

//    //mapping status
    @ManyToOne
    @JoinColumn(name = "status_id", referencedColumnName = "status_id")
    @ToString.Exclude
    private Status status;


//   mapping interview schedule
    @OneToOne(mappedBy = "candidate")
    private InterviewSchedule interviewSchedule;

//    //mapping offer
    @OneToOne(mappedBy = "candidate")
    private Offer offer;

    @ManyToOne
    @JoinColumn(name = "recruiter_id", referencedColumnName = "user_id")
    @ToString.Exclude
    private User recruiter;
}
