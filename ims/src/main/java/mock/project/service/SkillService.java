package mock.project.service;

import mock.project.entities.Skill;

import java.util.List;
import java.util.Optional;

public interface SkillService {
    List<Skill> findAllSkill();

    Optional<Skill> findSkillById(Long id);

    Optional<Skill> findSkillBySkillName(String skillName);

}
