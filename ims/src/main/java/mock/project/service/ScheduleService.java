package mock.project.service;

import mock.project.entities.InterviewSchedule;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ScheduleService {
    InterviewSchedule saveSchedule(InterviewSchedule schedule);

    List<InterviewSchedule> findAllSchedule();

    Optional<InterviewSchedule> findScheduleById(Long id);

    void deleteScheduleById(Long id);

    Page<InterviewSchedule> findPaginated(int pageNo, int pageSize);

    List<String> findInterviewerUsernamesByCandidateId(@Param("candidateId") Long candidateId);

    List<InterviewSchedule> findAllByTitleContainingIgnoreCaseOrStatusName(String title, String statusName);

    List<InterviewSchedule> findAllByTitleContainingIgnoreCaseAndStatusName(String title, String statusName);

    void deleteInterviewersScheduleByScheduleId(Long id);
}
