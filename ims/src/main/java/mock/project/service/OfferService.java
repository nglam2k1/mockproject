package mock.project.service;

import mock.project.entities.Offer;
import org.springframework.data.domain.Page;
import java.util.List;
import java.util.Optional;

public interface OfferService{
    Page<Offer> findPaginated(int pageNo, int pageSize);
    Offer saveOffer(Offer offer);

    List<Offer> findAllOffer();

    Optional<Offer> findOfferById(Long id);

    void deleteOfferById(Long id);

    List<Offer> findAllByStatusNameAndDepartment_DeptNameAndCandidateFullNameContainingIgnoreCase(String statusName, String deptName, String fullName);

    List<Offer> findAllByStatusNameOrDepartment_DeptNameOrCandidateFullNameContainingIgnoreCase(String statusName, String deptName, String fullName);
}
