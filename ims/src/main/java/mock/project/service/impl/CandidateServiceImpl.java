package mock.project.service.impl;

import mock.project.entities.Candidate;
import mock.project.repository.CandidateRepository;
import mock.project.service.CandidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class CandidateServiceImpl implements CandidateService {

    private final CandidateRepository candidateRepository;

    @Autowired
    public CandidateServiceImpl(CandidateRepository candidateRepository) {
        this.candidateRepository = candidateRepository;
    }

    @Override
    public Candidate saveCandidate(Candidate candidate) {
        return candidateRepository.save(candidate);
    }

    @Override
    public List<Candidate> findAllCandidate() {
        return candidateRepository.findAll();
    }

    @Override
    public Optional<Candidate> findCandidateById(Long id) {
        return candidateRepository.findById(id);
    }

    @Override
    public void deleteCandidateById(Long id) {
        candidateRepository.deleteById(id);
    }

    @Override
    public Optional<Candidate> findCandidateByFullName(String fullName) {
        return candidateRepository.findCandidateByFullName(fullName);
    }

    @Override
    public Page<Candidate> findPaginated(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
        return candidateRepository.findAll(pageable);
    }

    @Override
    public void deleteCandidateSkillByCandidateId(@Param("id") Long id) {
        candidateRepository.deleteCandidateSkillByCandidateId(id);
    }

    @Override
    public Optional<Candidate> findCandidateByEmail(String email) {
        return candidateRepository.findCandidateByEmail(email);
    }

    @Override
    public boolean existsByEmail(String email) {
        return candidateRepository.existsByEmail(email);
    }

    @Override
    public boolean existsByPhone(String phone) {
        return candidateRepository.existsByPhone(phone);
    }

    @Override
    public List<Candidate> findAllByFullNameContainingIgnoreCaseAndStatusName(String fullName, String status) {
        return candidateRepository.findAllByFullNameContainingIgnoreCaseAndStatusName(fullName, status);
    }

    @Override
    public List<Candidate> findAllByStatusNameOrFullNameContainingIgnoreCase(String status, String fullName) {
        return candidateRepository.findAllByStatusNameOrFullNameContainingIgnoreCase(status, fullName);
    }

    @Override
    public List<Candidate> findAllByStatusName(String statusName) {
        return candidateRepository.findAllByStatusName(statusName);
    }

    @Override
    public List<Candidate> findAllCandidateByStatusNameAndOfferIsNull(String statusName) {
        return candidateRepository.findAllByStatusNameAndOfferIsNull(statusName);
    }

//    @Override
//    public List<Candidate> findByStatusAndNoOffer(String statusName) {
//        return candidateRepository.findByStatusAndNoOffer(statusName);
//    }
}
