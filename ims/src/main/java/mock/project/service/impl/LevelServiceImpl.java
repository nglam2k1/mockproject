package mock.project.service.impl;

import mock.project.entities.Level;
import mock.project.repository.LevelRepository;
import mock.project.service.LevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class LevelServiceImpl implements LevelService {
    private final LevelRepository levelRepository;

    @Autowired
    public LevelServiceImpl(LevelRepository levelRepository) {
        this.levelRepository = levelRepository;
    }

    @Override
    public List<Level> findAllLevel() {
        return levelRepository.findAll();
    }

    @Override
    public Optional<Level> findLevelById(Long id) {
        return levelRepository.findById(id);
    }

    @Override
    public Optional<Level> findLevelByLevelName(String levelName) {
        return levelRepository.findByLevelName(levelName);
    }


}
