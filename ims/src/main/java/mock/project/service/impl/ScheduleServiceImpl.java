package mock.project.service.impl;

import mock.project.entities.InterviewSchedule;
import mock.project.repository.ScheduleRepository;
import mock.project.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ScheduleServiceImpl implements ScheduleService {
    private final ScheduleRepository scheduleRepository;

    @Autowired
    public ScheduleServiceImpl(ScheduleRepository scheduleRepository) {
        this.scheduleRepository = scheduleRepository;
    }

    @Override
    public InterviewSchedule saveSchedule(InterviewSchedule schedule) {
        return scheduleRepository.save(schedule);
    }

    @Override
    public List<InterviewSchedule> findAllSchedule() {
        return scheduleRepository.findAll();
    }

    @Override
    public Optional<InterviewSchedule> findScheduleById(Long id) {
        return scheduleRepository.findById(id);
    }

    @Override
    public void deleteScheduleById(Long id) {
        scheduleRepository.deleteById(id);
    }

    @Override
    public Page<InterviewSchedule> findPaginated(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
        return scheduleRepository.findAll(pageable);
    }

    @Override
    public List<InterviewSchedule> findAllByTitleContainingIgnoreCaseOrStatusName(String title, String statusName) {
        return scheduleRepository.findAllByTitleContainingIgnoreCaseOrStatusName(title, statusName);
    }

    @Override
    public List<InterviewSchedule> findAllByTitleContainingIgnoreCaseAndStatusName(String title, String statusName) {
        return scheduleRepository.findAllByTitleContainingIgnoreCaseAndStatusName(title, statusName);
    }

    @Override
    public void deleteInterviewersScheduleByScheduleId(Long id) {
        scheduleRepository.deleteInterviewersScheduleByScheduleId(id);
    }

    @Override
    public List<String> findInterviewerUsernamesByCandidateId(Long candidateId) {
        return scheduleRepository.findInterviewerUsernamesByCandidateId(candidateId);
    }
}
