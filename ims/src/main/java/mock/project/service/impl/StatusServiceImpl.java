package mock.project.service.impl;

import mock.project.entities.Status;
import mock.project.entities.enums.StatusType;
import mock.project.repository.StatusRepository;
import mock.project.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class StatusServiceImpl implements StatusService {

    private final StatusRepository statusRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    public StatusServiceImpl(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Override
    public Optional<Status> findStatusByName(String name) {
        return statusRepository.findByName(name);
    }


    @Override
    public List<Status> findAllStatusByStatusType(StatusType statusType) {
        return statusRepository.findAllByStatusType(statusType);
    }

    @Override
    public Optional<Status> findByNameAndStatusType(String name, StatusType statusType) {
        return statusRepository.findByNameAndStatusType(name, statusType);
    }

}
