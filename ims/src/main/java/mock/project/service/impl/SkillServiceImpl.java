package mock.project.service.impl;

import mock.project.entities.Skill;
import mock.project.repository.SkillRepository;
import mock.project.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import java.util.Optional;


@Service
@Transactional
public class SkillServiceImpl implements SkillService {
    private final SkillRepository skillRepository;

    @Autowired
    public SkillServiceImpl(SkillRepository skillRepository) {
        this.skillRepository = skillRepository;
    }

    @Override
    public List<Skill> findAllSkill() {
        return skillRepository.findAll();
    }


    @Override
    public Optional<Skill> findSkillById(Long id) {
        return skillRepository.findById(id);
    }

    @Override
    public Optional<Skill> findSkillBySkillName(String skillName) {
        return skillRepository.findBySkillName(skillName);
    }

}
