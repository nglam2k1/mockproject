package mock.project.service.impl;


import mock.project.entities.Benefits;
import mock.project.repository.BenefitsRepository;
import mock.project.service.BenefitsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class BenefitsServiceImpl implements BenefitsService {
    private final BenefitsRepository benefitsRepository;

    @Autowired
    public BenefitsServiceImpl(BenefitsRepository benefitsRepository) {
        this.benefitsRepository = benefitsRepository;
    }

    @Override
    public List<Benefits> findAllBenefit() {
        return benefitsRepository.findAll();
    }

    @Override
    public Optional<Benefits> findBenefitById(Long id) {
        return benefitsRepository.findById(id);
    }

    @Override
    public Optional<Benefits> findBenefitByBenefitName(String benefitName) {
        return benefitsRepository.findByBenefitName(benefitName);
    }


}
