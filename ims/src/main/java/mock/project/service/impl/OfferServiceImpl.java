package mock.project.service.impl;

import mock.project.entities.Offer;
import mock.project.repository.OfferRepository;
import mock.project.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class OfferServiceImpl implements OfferService {

    private final OfferRepository offerRepository;

    @Autowired
    public OfferServiceImpl(OfferRepository offerRepository) {
        this.offerRepository = offerRepository;
    }

    @Override
    public Page<Offer> findPaginated(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
        return offerRepository.findAll(pageable);
    }

    @Override
    public Offer saveOffer(Offer offer) {
        return offerRepository.save(offer);
    }

    @Override
    public List<Offer> findAllOffer() {
        return offerRepository.findAll();
    }

    @Override
    public Optional<Offer> findOfferById(Long id) {
        return offerRepository.findById(id);
    }

    @Override
    public void deleteOfferById(Long id) {
        offerRepository.deleteById(id);
    }

    @Override
    public List<Offer> findAllByStatusNameAndDepartment_DeptNameAndCandidateFullNameContainingIgnoreCase(String statusName, String deptName, String fullName) {
        return offerRepository.findAllByStatusNameAndDepartment_DeptNameAndCandidateFullNameContainingIgnoreCase(statusName, deptName, fullName);
    }

    @Override
    public List<Offer> findAllByStatusNameOrDepartment_DeptNameOrCandidateFullNameContainingIgnoreCase(String statusName, String deptName, String fullName) {
        return offerRepository.findAllByStatusNameOrDepartment_DeptNameOrCandidateFullNameContainingIgnoreCase(statusName, deptName, fullName);
    }

}
