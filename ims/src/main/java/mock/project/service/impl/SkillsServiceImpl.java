package mock.project.service.impl;

import mock.project.entities.Skill;
import mock.project.repository.SkillsRepository;
import mock.project.service.SkillsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class SkillsServiceImpl implements SkillsService {
    private final SkillsRepository skillsRepository;

    @Autowired
    public SkillsServiceImpl(SkillsRepository skillsRepository) {
        this.skillsRepository = skillsRepository;
    }

    @Override
    public List<Skill> findAllSkills() {
        return skillsRepository.findAll();
    }

    @Override
    public Optional<Skill> findSkillById(Long id) {
        return skillsRepository.findById(id);
    }

    @Override
    public Optional<Skill> findSkillBySkillName(String skillName) {
        return skillsRepository.findBySkillName(skillName);
    }
}
