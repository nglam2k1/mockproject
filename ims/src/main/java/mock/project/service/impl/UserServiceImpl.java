package mock.project.service.impl;

import mock.project.entities.User;
import mock.project.entities.enums.UserRole;
import mock.project.repository.UserRepository;
import mock.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final JavaMailSender javaMailSender;
    @Autowired
    public UserServiceImpl(UserRepository userRepository, JavaMailSender javaMailSender) {
        this.userRepository = userRepository;
        this.javaMailSender = javaMailSender;
    }

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> findAllUser() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> findUserById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public Optional<User> findUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Page<User> findPaginated(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
        return userRepository.findAll(pageable);
    }

    @Override
    public Optional<User> findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public boolean existsByPhoneNumber(String phone) {
        return userRepository.existsByPhoneNumber(phone);
    }

    @Override
    public List<User> findAllUserByUsernameContainingIgnoreCaseAndRole(String username, UserRole role) {
        return userRepository.findAllByUsernameContainingIgnoreCaseAndRole(username, role);
    }

    @Override
    public List<User> findAllUserByRoleOrUsernameContainingIgnoreCase(UserRole role, String username) {
        return userRepository.findAllByRoleOrUsernameContainingIgnoreCase(role, username);
    }

    @Override
    public void sendAccountToUser(String to, String username, String password) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(to);
        mailMessage.setSubject("no-reply-email-IMS-system <Account created>");
        mailMessage.setText("This email is from IMS system,\n\n" +
                "Your account has been created. Please use the following credentials to login:\n" +
                "• User name: " + username + "\n" +
                "• Password: " + password + "\n" +
                "If anything is wrong, please reach out to the recruiter at <offer recruiter owner account>." +
                " We are sorry for any inconvenience.");
        javaMailSender.send(mailMessage);
    }

    @Override
    public List<User> findAllUsersByRole(UserRole role) {
        return userRepository.findAllByRole(UserRole.MANAGER);
    }

    @Override
    public void sendNewPasswordToUser(String to, String username, String password) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(to);
        mailMessage.setSubject("no-reply-email-IMS-system <Account created>");
        mailMessage.setText("This email is from IMS system,\n\n" +
                "Your new password has been created. Please use the following credentials to login:\n" +
                "• User name: " + username + "\n" +
                "• New Password: " + password + "\n" +
                "If anything is wrong, please reach out to the recruiter at <offer recruiter owner account>." +
                " We are sorry for any inconvenience.");
        javaMailSender.send(mailMessage);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userOptional = userRepository.findByUsername(username);
        if (userOptional.isEmpty()) {
            throw new UsernameNotFoundException("User not found");
        }
        User user = userOptional.get();
        return new org.springframework.security.core.userdetails.User(user.getUsername(),
                user.getPassword(), Collections.singletonList(new SimpleGrantedAuthority("ROLE_" + user.getRole().name())));
    }

}
