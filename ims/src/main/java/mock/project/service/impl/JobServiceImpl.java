package mock.project.service.impl;

import mock.project.entities.Job;
import mock.project.repository.JobRepository;
import mock.project.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class JobServiceImpl implements JobService {

    private final JobRepository jobRepository;

    @Autowired
    public JobServiceImpl(JobRepository jobRepository) {
        this.jobRepository = jobRepository;
    }

    @Override
    public Job saveJob(Job job) {
        return jobRepository.save(job);
    }

    @Override
    public List<Job> findAllJob() {
        return jobRepository.findAll();
    }

    @Override
    public Optional<Job> findJobById(Long id) {
        return jobRepository.findById(id);
    }

    @Override
    public void deleteJobById(Long id) {
        jobRepository.deleteById(id);
    }

    @Override
    public Page<Job> findPaginated(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
        return jobRepository.findAll(pageable);
    }

    @Override
    public void deleteJobSkillByJobId(Long id) {
        jobRepository.deleteJobSkillByJobId(id);
    }

    @Override
    public void deleteJobLevelByJobId(Long id) {
        jobRepository.deleteJobLevelByJobId(id);
    }

    @Override
    public void deleteJobBenefitsByJobId(Long id) {
        jobRepository.deleteJobBenefitsByJobId(id);
    }

    @Override
    public boolean existsByTitle(String title) {
        return jobRepository.existsByTitle(title);
    }

    @Override
    public List<Job> findAllByTitleContainingIgnoreCaseAndStatusName(String title, String status) {
        return jobRepository.findAllByTitleContainingIgnoreCaseAndStatusName(title, status);
    }

    @Override
    public List<Job> findAllByStatusNameOrTitleContainingIgnoreCase(String status, String title) {
        return jobRepository.findAllByStatusNameOrTitleContainingIgnoreCase(status, title);
    }



}
