package mock.project.service;

import mock.project.entities.Position;

import java.util.List;
import java.util.Optional;

public interface PositionService {
    List<Position> findAllPositions();

    Optional<Position> findPositionById(Long id);

    Optional<Position> findPositionByName(String name);
}
