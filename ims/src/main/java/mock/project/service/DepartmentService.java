package mock.project.service;

import mock.project.entities.Department;

import java.util.List;
import java.util.Optional;

public interface DepartmentService {
    List<Department> findAllDepartment();

    Optional<Department> findDepartmentById(Long id);

    Optional<Department> findDepartmentByDeptName(String deptName);
}
