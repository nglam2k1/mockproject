package mock.project.service;

import mock.project.entities.Level;

import java.util.List;
import java.util.Optional;

public interface LevelService {

    List<Level> findAllLevel();

    Optional<Level> findLevelById(Long id);

    Optional<Level> findLevelByLevelName(String levelName);


}
