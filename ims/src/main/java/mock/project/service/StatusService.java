package mock.project.service;

import mock.project.entities.Status;
import mock.project.entities.enums.StatusType;

import java.util.List;
import java.util.Optional;

public interface StatusService {

    Optional<Status> findStatusByName(String name);

    List<Status> findAllStatusByStatusType(StatusType statusType);

    Optional<Status> findByNameAndStatusType(String name, StatusType statusType);

}

