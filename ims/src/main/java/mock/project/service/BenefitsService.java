package mock.project.service;

import mock.project.entities.Benefits;

import java.util.List;
import java.util.Optional;

public interface BenefitsService {
    List<Benefits> findAllBenefit();

    Optional<Benefits> findBenefitById(Long id);

    Optional<Benefits> findBenefitByBenefitName(String benefitName);

}
