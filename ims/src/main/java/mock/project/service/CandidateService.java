package mock.project.service;

import mock.project.entities.Candidate;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import java.util.List;
import java.util.Optional;

public interface CandidateService {
    Candidate saveCandidate(Candidate candidate);

    List<Candidate> findAllCandidate();

    Optional<Candidate> findCandidateById(Long id);

    void deleteCandidateById(Long id);

    Optional<Candidate> findCandidateByFullName(String fullName);

    Page<Candidate> findPaginated(int pageNo, int pageSize);

    void deleteCandidateSkillByCandidateId(@Param("id") Long id);

    Optional<Candidate> findCandidateByEmail(String email);

    boolean existsByEmail(String email);

    boolean existsByPhone(String phoneNumber);

    List<Candidate> findAllByFullNameContainingIgnoreCaseAndStatusName(String fullName, String status);

    List<Candidate> findAllByStatusNameOrFullNameContainingIgnoreCase(String status, String fullName);

    List<Candidate> findAllByStatusName(String statusName);

    List<Candidate> findAllCandidateByStatusNameAndOfferIsNull(String statusName);

}
