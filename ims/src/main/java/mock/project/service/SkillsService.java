package mock.project.service;

import mock.project.entities.Skill;

import java.util.List;
import java.util.Optional;

public interface SkillsService{
    List<Skill> findAllSkills();

    Optional<Skill> findSkillById(Long id);

    Optional<Skill> findSkillBySkillName(String deptName);

}
