package mock.project.service;

import mock.project.entities.Candidate;
import mock.project.entities.Job;
import mock.project.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface JobService {
    Job saveJob(Job job);

    List<Job> findAllJob();

    Optional<Job> findJobById(Long id);

    void deleteJobById(Long id);

    Page<Job> findPaginated(int pageNo, int pageSize);

    void deleteJobSkillByJobId(@Param("id") Long id);

    void deleteJobLevelByJobId(@Param("id") Long id);

    void deleteJobBenefitsByJobId(@Param("id") Long id);

    boolean existsByTitle(String title);

    List<Job> findAllByTitleContainingIgnoreCaseAndStatusName(String title, String status);

    List<Job> findAllByStatusNameOrTitleContainingIgnoreCase(String status, String title);

}
