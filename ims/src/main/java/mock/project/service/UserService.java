package mock.project.service;

import mock.project.entities.User;
import mock.project.entities.enums.UserRole;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Optional;

public interface UserService extends UserDetailsService {

    User saveUser(User user);

    List<User> findAllUser();

    Optional<User> findUserById(Long id);

    void deleteUserById(Long id);

    Page<User> findPaginated(int pageNo, int pageSize);

    Optional<User> findUserByEmail(String email);

    boolean existsByEmail(String email);

    boolean existsByPhoneNumber(String phone);

    List<User> findAllUserByUsernameContainingIgnoreCaseAndRole(String username, UserRole role);

    List<User> findAllUserByRoleOrUsernameContainingIgnoreCase(UserRole role, String username);

    Optional<User> findUserByUsername(String username);

    void sendAccountToUser(String to, String username, String password);

    List<User> findAllUsersByRole(UserRole role);

    void sendNewPasswordToUser(String to, String username, String password);

}
