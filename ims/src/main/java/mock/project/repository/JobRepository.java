package mock.project.repository;

import mock.project.entities.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobRepository extends JpaRepository<Job, Long> {

    @Modifying
    @Query(value = "DELETE FROM job_skill WHERE jobs_job_id = :id", nativeQuery = true)
    void deleteJobSkillByJobId(@Param("id") Long id);

    @Modifying
    @Query(value = "DELETE FROM job_level WHERE jobs_job_id = :id", nativeQuery = true)
    void deleteJobLevelByJobId(@Param("id") Long id);

    @Modifying
    @Query(value = "DELETE FROM job_benefits WHERE jobs_job_id = :id", nativeQuery = true)
    void deleteJobBenefitsByJobId(@Param("id") Long id);

    boolean existsByTitle(String title);

    List<Job> findAllByTitleContainingIgnoreCaseAndStatusName(String title, String status);

    List<Job> findAllByStatusNameOrTitleContainingIgnoreCase(String status, String title);

}
