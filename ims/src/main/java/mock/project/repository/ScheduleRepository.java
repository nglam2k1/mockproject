package mock.project.repository;

import mock.project.entities.InterviewSchedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScheduleRepository extends JpaRepository<InterviewSchedule, Long> {
    @Query("SELECT u.username FROM InterviewSchedule i " +
            "JOIN i.interviewers u " +
            "WHERE i.candidate.id = :candidateId")
    List<String> findInterviewerUsernamesByCandidateId(@Param("candidateId") Long candidateId);

    List<InterviewSchedule> findAllByTitleContainingIgnoreCaseOrStatusName(String title, String statusName);

    List<InterviewSchedule> findAllByTitleContainingIgnoreCaseAndStatusName(String title, String statusName);

    @Modifying
    @Query(value = "DELETE FROM schedule_interviewer WHERE schedule_id = :id", nativeQuery = true)
    void deleteInterviewersScheduleByScheduleId(@Param("id") Long id);
}
