package mock.project.repository;

import mock.project.entities.Benefits;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BenefitsRepository extends JpaRepository<Benefits, Long> {
    Optional<Benefits> findByBenefitName(String benefitName);

}
