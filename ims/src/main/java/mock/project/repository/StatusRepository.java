package mock.project.repository;

import mock.project.entities.Status;
import mock.project.entities.enums.StatusType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StatusRepository extends JpaRepository<Status, Long> {
    Optional<Status> findByName(String name);

    List<Status> findAllByStatusType(StatusType statusType);

    Optional<Status> findByNameAndStatusType(String name, StatusType statusType);

}
