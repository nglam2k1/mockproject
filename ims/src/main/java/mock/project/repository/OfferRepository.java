package mock.project.repository;

import mock.project.entities.Offer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OfferRepository extends JpaRepository<Offer, Long> {
    List<Offer> findByDepartment_DeptName(String deptName);

    List<Offer> findAllByStatusNameAndDepartment_DeptNameAndCandidateFullNameContainingIgnoreCase(String statusName, String deptName, String fullName);

    List<Offer> findAllByStatusNameOrDepartment_DeptNameOrCandidateFullNameContainingIgnoreCase(String statusName, String deptName, String fullName);
}
