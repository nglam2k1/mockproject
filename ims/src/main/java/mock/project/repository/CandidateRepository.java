package mock.project.repository;

import mock.project.entities.Candidate;
import mock.project.entities.Status;
import mock.project.entities.User;
import mock.project.entities.enums.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CandidateRepository extends JpaRepository<Candidate, Long> {
    @Modifying
    @Query(value = "DELETE FROM candidate_skill WHERE candidates_candidate_id = :id", nativeQuery = true)
    void deleteCandidateSkillByCandidateId(@Param("id") Long id);

    List<Candidate> findAllByStatusNameAndOfferIsNull(String statusName);

    boolean existsByEmail(String email);

    boolean existsByPhone(String phone);

    Optional<Candidate> findCandidateByEmail(String email);

    Optional<Candidate> findCandidateByFullName(String fullName);

    List<Candidate> findAllByStatusName(String statusName);

    List<Candidate> findAllByFullNameContainingIgnoreCaseAndStatusName(String fullName, String statusName);

    List<Candidate> findAllByStatusNameOrFullNameContainingIgnoreCase(String statusName, String fullName);
}













