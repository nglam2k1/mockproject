package mock.project.repository;

import mock.project.entities.User;
import mock.project.entities.enums.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    List<User> findAllByRole(UserRole role);

    Optional<User> findByEmail(String email);

    boolean existsByEmail(String email);

    @Query("SELECT CASE WHEN COUNT(u) > 0 THEN true ELSE false END " +
            "FROM User u WHERE u.phoneNumber = :phone AND u.phoneNumber IS NOT NULL AND u.phoneNumber <> ''")
    boolean existsByPhoneNumber(@Param("phone") String phone);

    List<User> findAllByUsernameContainingIgnoreCaseAndRole(String username, UserRole role);

    List<User> findAllByRoleOrUsernameContainingIgnoreCase(UserRole role, String username);

}
