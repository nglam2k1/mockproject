package mock.project.repository;

import mock.project.entities.Skill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SkillsRepository extends JpaRepository<Skill, Long> {
        Optional<Skill> findBySkillName(String skillName);
}
