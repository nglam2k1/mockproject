package mock.project.utils;

import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserUtils {

    public String autoGenerateUsername(String fullName, List<String> existedUsername) {
        StringBuilder result = new StringBuilder();
        StringBuilder inputBuilder = new StringBuilder(fullName);

        String[] words = fullName.split(" ");
        if (words.length > 0) {
            String lastWord = words[words.length - 1];
            result.append(lastWord);

            int startIndex = inputBuilder.indexOf(lastWord);
            int endIndex = startIndex + lastWord.length();
            if (startIndex != -1) {
                inputBuilder.delete(startIndex, endIndex);
            }

            boolean isFirstChar = true;

            for (int i = 0; i < inputBuilder.length(); i++) {
                char currentChar = inputBuilder.charAt(i);

                if (Character.isWhitespace(currentChar)) {
                    isFirstChar = true;
                } else if (isFirstChar) {
                    result.append(currentChar);
                    isFirstChar = false;
                }
            }
        }

        String usernameToSearch = result.toString().toLowerCase();
        List<Integer> newList = existedUsername.stream()
                .filter(username -> username.toLowerCase().startsWith(usernameToSearch))
                .map(UserUtils::getLastNumberFromString)
                .filter(lastNumber -> lastNumber != -1)
                .collect(Collectors.toList());

        if (!newList.isEmpty()) {
            int maxNumber = newList.stream().mapToInt(Integer::intValue).max().orElse(0);
            result.append(maxNumber + 1);
        } else {
            result.append("1");
        }

        return result.toString();
    }

    public static int getLastNumberFromString(String input) {
        int lastDigitIndex = input.length() - 1;
        while (lastDigitIndex >= 0 && Character.isDigit(input.charAt(lastDigitIndex))) {
            lastDigitIndex--;
        }

        if (lastDigitIndex < input.length() - 1) {
            String lastNumberString = input.substring(lastDigitIndex + 1);
            return Integer.parseInt(lastNumberString);
        } else {
            return -1;
        }
    }


    public String autoGeneratePassword(int length) {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()-_=+";
        StringBuilder password = new StringBuilder();
        SecureRandom random = new SecureRandom();
        for (int i = 0; i < length; i++) {
            int randomIndex = random.nextInt(characters.length());
            password.append(characters.charAt(randomIndex));
        }
        return password.toString();
    }
}
